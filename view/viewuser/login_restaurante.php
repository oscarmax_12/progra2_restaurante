
<?php
	session_start();
	$tipo_usuario = 0;
	if (isset($_SESSION['tipo_usuario'])) {
        $tipo_usuario = $_SESSION['tipo_usuario'];            
	}

	$idrestaurante ="";
	$nombre="";
	$dir ="";
	$tel ="";
	$cap ="";
	$des = "";
	$itc = "";
	$img = "";

	if($tipo_usuario==2){
		header("location: login_usuario.php");

	}else{
		if(isset($_SESSION['id'])){
			$idrestaurante = $_SESSION['id'];
			$nombre = $_SESSION['nombre']; 
			$dir = $_SESSION['direccion']; 
			$tel = $_SESSION['telefono']; 
			$cap = $_SESSION['capacidad']; 
			$des = $_SESSION['descripcion'];
			$itc = $_SESSION['idtipocomida'];
			$img = $_SESSION['imagen'];

			header("location: index.php");
		}
	}

?>


<!DOCTYPE html>
<html>
<head>
<title>BusFood | Inicio de sesión de restaurante</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Custom Theme files -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfont-->
<link href='//fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Lobster+Two:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<!--Animation-->
<script src="js/wow.min.js"></script>
<link href="css/animate.css" rel='stylesheet' type='text/css' />

<link href="css/loginRestaurante.css" rel="stylesheet" type="text/css" media="all" />


<script>
	new WOW().init();
</script>
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
				});
			});
		</script>
<script src="js/simpleCart.min.js"> </script>	
</head>
<body>
    <!-- header-section-starts -->
	<div class="header">
		<div class="container">
			<div class="top-header">
				<div class="logo">
					<a href="index.html"><img src="images/logo.png" class="img-responsive" alt="" /></a>
				</div>
				<div class="queries">
					<p>Questions? Call us Toll-free!<span>1800-0000-7777 </span><label>(11AM to 11PM)</label></p>
				</div>
				<div class="header-right">
						<div class="cart box_1">
							
							<?php 
								if (!isset($_SESSION['id'])){
										echo '';
									};
								?> 
									
								<?php 
									if (isset($_SESSION['id'])){
										echo '<h3> Restaurante '.$nombre.'
												<img src="'.$img.'" style="width: 100px"></h3>
												<a href="checkout.html">
												</a>';
								};
							?> 

							<div class="clearfix"> </div>
						</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
			<div class="menu-bar">
			<div class="container">
				<div class="top-menu">
					<ul>
						<li><a href="index.php">Home</a></li>
						<li><a href="restaurants.php">Restaurantes</a></li>
						<li><a href="reservar.php">Reserva</a></li>
						<li><a href="contact.html">contacto</a></li>
						<li><a href="misreservas.php">Mis reservas</a></li>
						<div class="clearfix"></div>
					</ul>
				</div>
				<div class="login-section">
					<ul>
						
						<li><a href="#">Login Restaurante</a>  </li> 
						<li><a href="login_usuario.php">Login Usuario</a>  </li> 
						<div class="clearfix"></div>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- header-section-ends -->
	<!-- content-section-starts -->
	<div class="container">
		<div class="row">
		<div class="col-md-4 login-sec">
		    <h2 class="text-center">Inicio de sesión de Restaurantes</h2>
		    <form class="login-form" action="php/login_restaurante.php" method="post">
  <div class="form-group">
    <label for="exampleInputEmail1" class="text-uppercase">Usuario</label>
    <input type="text" name="txtusuario" id="txtusuario" class="form-control" placeholder="">
    
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1" class="text-uppercase">Contraseña</label>
    <input type="password" name="txtcontra" id="txtcontra" class="form-control" placeholder="">
  </div>
  
  
    <div class="form-check">
    
    <button type="submit" class="btn btn-login float-right">Entrar</button><br><br>
    <label class="form-check-label">
      <small>Aún no estas registrado?</small>
		
	<a class="acount-btn" href="register_restaurante.php">Create una cuenta</a>
    </label>
  </div>
  
</form>

		</div>
		<div class="col-md-8 banner-sec">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                 <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                  </ol>
            
    <div class="carousel-item active">
      <img class="d-block img-fluid" src="images/logo_restaurante.jpg" alt="">
      <div class="carousel-caption d-none d-md-block">
        	
  </div>
    </div>
    
              
		    
		</div>
	</div>
	</div>
	
	
	<div class="footer">
		<div class="container">
			<p class="wow fadeInLeft" data-wow-delay="0.4s">&copy; 2018  Desarrollo reservado |  &nbsp;<a href="#" target="target_blank">Proyecto Programación II</a></p>
		</div>
	</div>
	<!-- footer-section-ends -->
	<script type="text/javascript">
		$(document).ready(function() {
							/*
							var defaults = {
					  			containerID: 'toTop', // fading element id
								containerHoverID: 'toTopHover', // fading element hover id
								scrollSpeed: 1200,
								easingType: 'linear' 
					 		};
							*/
							
		$().UItoTop({ easingType: 'easeOutQuart' });
							
		});
	</script>
	<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	<script src="js/funciones.js"></script>

</body>
</html>