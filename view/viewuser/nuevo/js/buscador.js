$("#buscar").keyup(function(e){
    e.preventDefault();
    var valor_buscado= $("#buscar").val();
    //DEFINIENDO LA CANTIDAD MINIMO DE LETRAS A INTRODUCIR PARA LA BUSQUEDA
    
    if(valor_buscado.length > 0){
        $('#lsteventosBuscador').removeClass('d-none');
        buscarEvento(valor_buscado);
    }else{
        $('#lsteventosBuscador').addClass('d-none');
    }
    
    
    
});

function buscarEvento(valor){
    var ruta= DIRECCION_WS+"evento.listar.autocompletar.php";
    $.ajax(
            {
                url: ruta,
                method:'POST',
                data:{
                    term:valor
                },
                success: function(data){
                    var datos=data;
//                    console.log(datos);
                    var html='';
                    $.each(datos,function(i,item){
                        
                        // html+='<li><a href="http://localhost:8080/proyecto_fiestas_today/paginas_web/usuario/detalleevento.php?even='+item.codigo_evento+'" style="display:inline-block; width:100%; text-decoration:none;"><img width="85" height="85" src="'+item.foto_flayer+'"><span class="ml-2 text-light">' +item.nombre_evento+'</span></a></li>';
                        
                        html+='<li><a href="https://www.fiestastoday.com/detalleevento.php?even='+item.codigo_evento+'" style="display:inline-block; width:100%; text-decoration:none;"><img width="85" height="85" src="'+item.foto_flayer+'"><span class="ml-2">' +item.nombre_evento+'</span></a></li>';
                        });
                    $('#lsteventosBuscador').html(html);
                },
                dataType:'json'
            }
           );
}
$(document).ready(function () {
 
    cargarComboTipoComida();
});

function cargarComboTipoComida( seccion) {
    $.post("../../backend/controladores/listar.combo.tipo.comida.php",
        // {
        //     p_sector: sector
        // }
    ).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "", selected;
            html += '<option value="*">Seleccione un Tipo de Comida</option>';
            $.each(datosJSON.datos, function (i, item) {
                selected = "";
                if (item.id === seccion) {
                    selected = ' selected=""';
                }
                html += '<option value="' + item.id + '"' + selected + '>' + item.descripcion + '</option>';
            });
            $("#txttipocomida").html(html);
        }
    }).fail(function (error) {
        var datosJSON = JSON.stringify(error);
        swal("Error", datosJSON.mensaje, "error");
    });
}
