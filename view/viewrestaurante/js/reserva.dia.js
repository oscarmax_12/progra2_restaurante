$(document).ready(function(){
    listar();
})

function listar(){
    const ruta ='../controladores/reserva.dia.listar.php';
    let user=window.localStorage.getItem('idUsuario')
    $.post(
        ruta,{
            p_codigo_usuario:user
        }
        

    ).done(function(resultado){

        if(resultado.estado==200){
            
            console.log(resultado);
            let datosJSON=resultado.datos;

            let html='';
            
            $.each(datosJSON, function (i, item) {
               
                html += '<tr>';
                html += '<td >' + item.id + '</td>';
                html += '<td >' + item.dia + '</td>';
                html += '<td >' + item.cantidad + '</td>';





               
                // html += '<td>                <button class="btn btn-danger" onclick="eliminar('+item.id+')">Eliminar</button>                <button class="btn btn-primary" onclick="editar('+item.id+')">Editar</button>            </td>';
                html += '<td><button class="btn btn-danger" onclick="eliminar('+item.id+')">Eliminar</button> <button class="btn btn-primary" onclick="leerdatos('+item.id+')">Editar</button></td>'; 
                
                

                html += '</tr>';
            });
            $('#tblUsuarios').html(html);
             $("#example1").DataTable();
          

            
             
        }else{
             console.log(resultado);
        }
        
        

        
    }).fail(function(error){
            console.log(error.responseJSON);
    })
}

function eliminar(id){
    const ruta="../controladores/usuario.eliminar.php";
    $.post(
        ruta,
        {
            p_codigo_usuario:id
        }
        

    ).done(function(resultado){

        if(resultado.estado==200){
            
            console.log(resultado);
            listar();
            
          

            
             
        }else{
             console.log(resultado);
        }
        
        

        
    }).fail(function(error){
            console.log(error.responseJSON);
    })
}

$('#btnagregarU').on('click',function(e){
    e.preventDefault();
    
    $('#txtCodigoUsuario').val("");
    $('#txtNombre').val("");
    $('#txtApellido').val("");
    $('#txtEmail').val("");
    $('#txtClave').val("");
    $('#txtTipoUsuario').val("");
    $('#imgFoto').val("");
    $('#txtEstado').val("");
    $('#operacion').val('agregar');
    $('#modal-usuarios').modal("show");
})


$('#btnGuardarUsuario').on('click',function(){
    if($('#operacion').val()=="agregar"){
        agregar();
        $('#modal-usuarios').modal("hide");
        $('#txtCodigoUsuario').val("");
        $('#txtNombre').val("");
        $('#txtApellido').val("");
        $('#txtEmail').val("");
        $('#txtClave').val("");
        $('#txtTipoUsuario').val("");
        $('#imgFoto').val("");
        $('#txtEstado').val("");        



        listar();
    }else{

        editar();
        $('#modal-usuarios').modal("hide");
        $('#txtCodigoUsuario').val("");
        $('#txtNombre').val("");
        $('#txtApellido').val("");
        $('#txtEmail').val("");
        $('#txtClave').val("");
        $('#txtTipoUsuario').val("");
        $('#imgFoto').val("");
        $('#txtEstado').val("");
        $('#operacion').val('')
        
    }
})


function agregar(){
    const ruta="../controladores/reserva.dia.agregar.php";

    let cant=$('#Cantidad').val();
    let desc=$('#descripcion').val();
    let fecha=$('#fecha').val()+' 10:00:00';
    let color='#FF0F0';
    let text='#FFFFFF';
    let user=window.localStorage.getItem('idUsuario')




    $.post(
        ruta,
        {
            p_fecha:fecha,
            p_descripcion:desc,
            p_color: color,
            p_text:text,
            p_id_usuario:user,
            p_cantidad:cant
        }
        

    ).done(function(resultado){

        if(resultado.estado==200){
            
            console.log(resultado);
            
        }else{
             console.log(resultado);
        }
        
    }).fail(function(error){
            console.log(error.responseJSON);
    })
}

function editar(){
    const ruta="../controladores/usuario.editar.editar.php";
    
    let codigo=$('#txtCodigoUsuario').val();
    let nombre=$('#txtNombre').val();
    let apellido=$('#txtApellido').val();
    let email=$('#txtEmail').val();
    let clave=$('#txtClave').val();
    let tipo_usuario=$('#txtTipoUsuario').val();
    let foto=$('#imgFoto').val();
    let estado=$('#txtEstado').val();



    $.post(
        ruta,
        {
            p_id_usuario:codigo,
            p_nombre:nombre,
            p_apellido:apellido,
            p_email:email,
            p_clave:clave,
            p_tipo_usuario:tipo_usuario,
            p_foto:foto,
            p_estado:estado

        }
        

    ).done(function(resultado){

        if(resultado.estado==200){
            
            
            listar();
        }else{
             console.log(resultado);
        }
        
    }).fail(function(error){
            console.log(error.responseJSON);
    })
}

function leerdatos(id){
    const ruta="../controladores/usuario.leerdatos.php";
    
    $.post(
        ruta,
        {
            p_codigo_usuario:id
        }
        

    ).done(function(resultado){

        if(resultado.estado==200){
            
            
            const datosJSON=resultado.datos;
            
            $('#txtCodigoUsuario').val(datosJSON.id);
            $('#txtNombre').val(datosJSON.nombre);
            $('#txtApellido').val(datosJSON.apellido);
            $('#txtEmail').val(datosJSON.email);
            $('#txtClave').val("");
            $('#txtTipoUsuario').val(datosJSON.tipo_usuario);
            // $('#imgFoto').val("");
            $('#txtEstado').val(datosJSON.estado);

            $('#operacion').val('editar');
            $('#modal-usuarios').modal("show");
        }else{
             console.log(resultado);
        }
        
    }).fail(function(error){
            console.log(error.responseJSON);
    })
}