$(document).ready(function(){
    listar();
});

function listar(){
    const ruta ='../controladores/tipocomida.listar.php';
    $.post(
        ruta,
        

    ).done(function(resultado){

        if(resultado.estado==200){
            
            // console.log(resultado);
            let datosJSON=resultado.datos;

            let html='';
            
            $.each(datosJSON, function (i, item) {
               
                html += '<tr>';
                html += '<td >' + item.id + '</td>';
                html += '<td >' + item.descripcion + '</td>';
               
                // html += '<td>                <button class="btn btn-danger" onclick="eliminar('+item.id+')">Eliminar</button>                <button class="btn btn-primary" onclick="editar('+item.id+')">Editar</button>            </td>';
                html += '<td><button class="btn btn-danger" onclick="eliminar('+item.id+')">Eliminar</button> <button class="btn btn-primary" onclick="leerdatos('+item.id+')">Editar</button></td>'; 
                
                

                html += '</tr>';
            });
            $('#tblTipoComida').html(html);
             $("#tbl").DataTable();
          

            
             
        }else{
             console.log(resultado);
        }
        
        

        
    }).fail(function(error){
            console.log(error.responseJSON);
    })
}

function eliminar(id){
    const ruta="../controladores/tipocomida.eliminar.php";
    $.post(
        ruta,
        {
            p_codigo_tipo_comida:id
        }
        

    ).done(function(resultado){

        if(resultado.estado==200){
            
            console.log(resultado);
            listar();
            
          

            
             
        }else{
             console.log(resultado);
        }
        
        

        
    }).fail(function(error){
            console.log(error.responseJSON);
    })

}
$('#btnagregarTC').on('click',function(e){
    e.preventDefault();
    
    $('#txtCodigoComida').val("");
    $('#txtComida').val("");
    $('#operacion').val('agregar');
    $('#modal-tipocomida').modal("show");
})


$('#btnGuardarTipoComida').on('click',function(){
    if($('#operacion').val()=="agregar"){
        agregarTipoComida();
        $('#modal-tipocomida').modal("hide");
        $('#txtComida').val('');
        $('#operacion').val('')
        listar();
    }else{

        editarTipoComida();
        $('#modal-tipocomida').modal("hide");
        $('#txtComida').val('');
        $('#operacion').val('')
        
    }
})

function agregarTipoComida(){
    const ruta="../controladores/tipocomida.agregar.php";
    let comida=$('#txtComida').val();
    $.post(
        ruta,
        {
            p_comida:comida
        }
        

    ).done(function(resultado){

        if(resultado.estado==200){
            
            console.log(resultado);
            
        }else{
             console.log(resultado);
        }
        
    }).fail(function(error){
            console.log(error.responseJSON);
    })
}

function editarTipoComida(){
    const ruta="../controladores/tipocomida.editar.php";
    let id_tipo_comida=$('#txtCodigoComida').val();
    let comida=$('#txtComida').val();
    $.post(
        ruta,
        {
            p_id_tipo_comida:id_tipo_comida,
            p_comida:comida

        }
        

    ).done(function(resultado){

        if(resultado.estado==200){
            
            
            listar();
        }else{
             console.log(resultado);
        }
        
    }).fail(function(error){
            console.log(error.responseJSON);
    })
}

function leerdatos(id){


    const ruta="../controladores/tipocomida.leerdatos.php";
    
    $.post(
        ruta,
        {
            p_codigo_tipo_comida:id
        }
        

    ).done(function(resultado){

        if(resultado.estado==200){
            
            
            const datosJSON=resultado.datos;
            $('#txtCodigoComida').val(datosJSON.id);
            $('#txtComida').val(datosJSON.descripcion);
            $('#operacion').val('editar');
            $('#modal-tipocomida').modal("show");
        }else{
             console.log(resultado);
        }
        
    }).fail(function(error){
            console.log(error.responseJSON);
    })


    
}