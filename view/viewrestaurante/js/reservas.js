$(document).ready(function(){
    
    $(document).ready(function () {
        // iniciarBotones();
        let user=window.localStorage.getItem('idUsuario')
        
        $('#calendario').fullCalendar({
            header: {
                // left:'today,prev,next, btnAgregar', AGREGAR EL NOMBRE DEL BOTON PERZONALIZADO PARA QUE FULL CALENDAR LO AGREGUE
                left: 'today,prev,next',
                center: 'title',
                right: 'month,basicWeek,basicDay,agendaWeek,year, agendaDay,listWeek, dayGridWeek,agendaFourDay',
                prev: 'left-single-arrow'
            },
            // customButtons:{
            //     btnAgregar:{
            //         text:'AGREGAR',
            //         click: function(){
            //             $('#modalEventos').modal();
            //         },
            //         prev: 'fa-chevron-left'
            //     }
            // },
            dayClick: function (date, jsEvent, view) {
                // alert('Clicked on: ' + date.format());
                // alert('Clicked on: ' + view.name);
                // alert('Clicked on: ' + date.format());
                // $(this).css('background-color','red')
                limpiarForm();
                $("#editar").prop("disabled",true)
                $("#eliminar").prop("disabled",true)
                $("#btnAgregar").prop("disabled",false)
                $('#txtFecha2').val(date.format())
                $('#modalEventos').modal(); //LANZAR UN MODAL
            },
            eventLimit:true,
            views:{
                agenda:{
                    eventLimit:4
                },
                agendaFourDay: {
                    type: 'agenda',
                    duration: { weeks:1  },
                    buttonText: 'Agenda de Semana',
                    nowIndicator:true
                  }
            },
            businessHours: [ // specify an array instead
                {
                  daysOfWeek: [ 1, 2, 3, 4, 5 ], // Monday, Tuesday, Wednesday
                  startTime: '08:00', // 8am
                  endTime: '18:00' // 6pm
                },
                {
                  daysOfWeek: [ 6, 7 ], // Thursday, Friday
                  startTime: '8:00', // 10am
                  endTime: '13:00' // 4pm
                }
              ],
            eventClick: function (callEvent, jsEvent, view) {
                $("#editar").prop("disabled",false)
                $("#eliminar").prop("disabled",false)
                $("#btnAgregar").prop("disabled",true)
                $('#txtTituloModal2').html(callEvent.title);
                // $('#txtDescripcionModal').html(callEvent.descripcion);
                // PARA EDITAR
                 $("#txtTitulo2").val(callEvent.title)
                $("#txtDescripcion2").val(callEvent.descripcion)
                $("#txtID2").val(callEvent.id)
                $("#txtColor2").val(callEvent.color)
                $("#cboEspecialidad").val(callEvent.codigo_especialidad)
                $("#cboEspecialidad").change();
                FechaHora=callEvent.start._i.split(" ");
    
                $("#txtFecha2").val(FechaHora[0]) 
                 $("#txtHora2").val(FechaHora[1])
    
                $('#modalEventos').modal();
    
            },
            events: '../controladores/calendario.cargar.datos.php?id='+user
        });
    })
    
  
})






