$(document).ready(function(e){
     
     if(
          window.localStorage.getItem('idUsuario') &&
          window.localStorage.getItem('fullname') &&
          window.localStorage.getItem('rest') &&
          window.localStorage.getItem('email')
     ){
          location.reload();
          location.href='home.php';
     }
});
$( "#frmSesion" ).submit(function( event ) {
     event.preventDefault();
     const ruta ='../controladores/sesion.php';
     let email=$('#txtEmail').val();
     let contrasena=$('#txtContrasena').val();


     

     $.post(
          ruta,
          {
               p_email:email,
               p_contrasena: contrasena

          }

     ).done(function(resultado){

          if(resultado.estado==200){
               var datosJSON=resultado.datos;
               // console.log(resultado);
               window.localStorage.setItem('idUsuario',datosJSON.id);
               window.localStorage.setItem('fullname',datosJSON.fullname);
               window.localStorage.setItem('rest',datosJSON.rest);
               window.localStorage.setItem('email',datosJSON.email);
               location.href='home.php';
          }else{
               console.log('faltan completar datos');
          }
          
          

          // location.href='home.php';
     }).fail(function(error){
          console.log(error.responseJSON.mensaje);
     })
     
   });