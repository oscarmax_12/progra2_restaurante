function cargarComboTipoComida(p_nombreCombo,p_tipo){
    const ruta = "../controladores/tipocomida.cargardatos.php";
    $.post
    (
	ruta
    ).done(function(resultado){
        
	var datosJSON = resultado;
	
        if (datosJSON.estado===200){
            
            var html = "";
            if (p_tipo==="seleccione"){
                
                html += '<option value="">Seleccione</option>';
            }else{
                html += '<option value="0">Todas los tipos de comida</option>';
            }

            
            $.each(datosJSON.datos, function(i,item) {
                
                html += '<option value="'+item.id+'">'+item.descripcion+'</option>';
            });
            
            $(p_nombreCombo).html(html);
	}else{
            console.log(resultado);
        }
    }).fail(function(error){
	var datosJSON = $.parseJSON( error.responseText );
	console.log(datosJSON.datos);
    });
}