$(document).ready(function(){
    
    listar();
    cargarComboTipoComida("#cboTipoComida","seleccione");
    cargarComboUsuario("#cboUsuario","seleccione");
})
function listar(){
    const ruta="../controladoresadmin/restaurante.listar.php";

    $.post(
        ruta
        

    ).done(function(resultado){

        if(resultado.estado==200){
            
            console.log(resultado);
            let datosJSON=resultado.datos;

            let html='';
            
            $.each(datosJSON, function (i, item) {
               
                html += '<tr>';
                html += '<td >' + item.id + '</td>';
                html += '<td >' + item.nombre + '</td>';
                html += '<td >' + item.direccion + '</td>';
                html += '<td >' + item.telefono + '</td>';
                html += '<td >' + item.capacidad + '</td>';
                html += '<td >' + item.descripcion + '</td>';
                html += '<td >' + item.comida + '</td>';
                html += '<td >' + item.imagen + '</td>';
                html += '<td >' + item.tipo_usuario + '</td>';
                html += '<td >' + item.usuario + '</td>';
                
               
                
                html += '<td><button class="btn btn-danger" onclick="eliminar('+item.id+')">Eliminar</button> <button class="btn btn-primary" onclick="leerdatos('+item.id+')">Editar</button></td>'; 
                
                

                html += '</tr>';
            });
            $('#tblRestaurante').html(html);
            $("#example1").DataTable();
            // $("#example1").DataTable({
            //     "scrollX": true
            // });
          

            
             
        }else{
             console.log(resultado);
        }
        
        

        
    }).fail(function(error){
            console.log(error.responseJSON);
    })
}


function eliminar(id){
    const ruta="../controladores/restaurante.eliminar.php";
    $.post(
        ruta,
        {
            p_codigo_restaurante:id
        }
        

    ).done(function(resultado){

        if(resultado.estado==200){
            
            listar();
        }else{
             console.log(resultado);
        }
        
    }).fail(function(error){
            console.log(error.responseJSON);
    })
}

$('#btnagregarR').on('click',function(e){
    e.preventDefault();
    
    $('#txtCodigoRestaurante').val("");
    $('#txtNombreRestaurante').val("");
    $('#txtDireccionRestaurante').val("");
    $('#txtTelefonoRestaurante').val("");
    $('#txtCapacidadRestaurante').val("");
    $('#txtDescripcionRestaurante').val("");
    $('#txtUsuarioRestaurante').val("");
    $('#txtClaveRestaurante').val("");

    $('#operacion').val('agregar');
    $('#modal-restaurante').modal("show");
})

$('#btnGuardarRestaurante').on('click',function(){
    if($('#operacion').val()=="agregar"){
        // agregarRestaurante();
        $('#modal-restaurante').modal("hide");
        
        $('#txtCodigoRestaurante').val("");
        $('#txtNombreRestaurante').val("");
        $('#txtDireccionRestaurante').val("");
        $('#txtTelefonoRestaurante').val("");
        $('#txtCapacidadRestaurante').val("");
        $('#txtDescripcionRestaurante').val("");
        $('#txtUsuarioRestaurante').val("");
        $('#txtClaveRestaurante').val("");


        $('#operacion').val('');
        listar();
    }else{

        editarRestaurante();
        $('#modal-restaurante').modal("hide");
        $('#txtCodigoRestaurante').val("");
        $('#txtNombreRestaurante').val("");
        $('#txtDireccionRestaurante').val("");
        $('#txtTelefonoRestaurante').val("");
        $('#txtCapacidadRestaurante').val("");
        $('#txtDescripcionRestaurante').val("");
        $('#txtUsuarioRestaurante').val("");
        $('#txtClaveRestaurante').val("");


        $('#operacion').val('');
        
    }
})

// function agregarRestaurante(){
//     const ruta="../controladoresadmin/restaurante.agregar.php";
//     let nombre_rest =$('#txtNombreRestaurante').val();
//     let direccion_rest=$('#txtDireccionRestaurante').val();
//     let telefono_rest=$('#txtTelefonoRestaurante').val();
//     let tipocomida_rest=$('#cboTipoComida').val();
//     let capacidad_rest=$('#txtCapacidadRestaurante').val();
//     let descripcion_rest=$('#txtDescripcionRestaurante').val();
//     // let imagen_rest=$('#imgRestaurante').val();
//     let imagen_rest=document.getElementById('imgRestaurante').files[0].name;

//     let usuario_rest=$('#txtUsuarioRestaurante').val();

//     let clave_rest=$('#txtClaveRestaurante').val();

    
//     $.post(
//         ruta,
//         {
//             p_nombre:nombre_rest,
//             p_direccion:direccion_rest,
//             p_telefono:telefono_rest,
//             p_tipocomida:tipocomida_rest,
//             p_capacidad:capacidad_rest,
//             p_descripcion:descripcion_rest,
//             p_imagen:imagen_rest,
//             p_usuario:usuario_rest,
//             p_clave:clave_rest

//         }
        

//     ).done(function(resultado){

//         if(resultado.estado==200){
            
//             console.log(resultado);
            
//         }else{
//              console.log(resultado);
//         }
        
//     }).fail(function(error){
//             console.log(error.responseJSON);
//     })

// }



$('#frmRestaurante').submit(function(e){
    e.preventDefault();
      
    var parametros = new FormData($("#frmRestaurante")[0]);
    // console.log(parametros);
  
    $.ajax({
        data:parametros,
        url:"../controladoresadmin/restaurante.agregar.php",
        type:"POST",
        contentType:false,
        processData: false,
        beforesend: function(){
  
        },
        success: function(response){
            console.log(response);
            // if(response.estado==200){
            //   location.href='inicio_sesion.php';
            // }else{
            //   alert('error al registrar usuario');
            // }
            switch (response) {
              case 'success':
                listar();
                break;
              
                case 'e-save':
                  alert('ERROR EN EL PROCESO DE GUARDADO: Error al agregar el registro');
                  break;
                case 'e-usuario':
                  alert('ERROR EN EL PROCESO DE GUARDADO: Error al agregar en los registro de USUARIO');
                  break;
                case 'e-persona':
                  alert('ERROR EN EL PROCESO DE GUARDADO: Error al agregar en los registro de PERSONA');
                  break;
                
            
              default:
                alert('ERROR EN EL PROCESO DE GUARDADO: Las contraseñas no coinciden');
                break;
            }
        }
    })
  })







function editarRestaurante(){
    const ruta="../controladoresadmin/restaurante.editar.php";

    let codigo_rest=$('#txtCodigoRestaurante').val();
    let nombre_rest =$('#txtNombreRestaurante').val();
    let direccion_rest=$('#txtDireccionRestaurante').val();
    let telefono_rest=$('#txtTelefonoRestaurante').val();
    let tipocomida_rest=$('#cboTipoComida').val();
    let capacidad_rest=$('#txtCapacidadRestaurante').val();
    let descripcion_rest=$('#txtDescripcionRestaurante').val();
    let imagen_rest=$('#imgRestaurante').val();

    let usuario_rest=$('#txtUsuarioRestaurante').val();

    let clave_rest=$('#txtClaveRestaurante').val();


    $.post(
        ruta,
        {
            p_codigo:codigo_rest,

            p_nombre:nombre_rest,
            p_direccion:direccion_rest,
            p_telefono:telefono_rest,
            p_tipocomida:tipocomida_rest,
            p_capacidad:capacidad_rest,
            p_descripcion:descripcion_rest,
            p_imagen:imagen_rest,
            p_usuario:usuario_rest,
            p_clave:clave_rest

        }
        

    ).done(function(resultado){

        if(resultado.estado==200){
            
            listar();
            
        }else{
             console.log(resultado);
        }
        
    }).fail(function(error){
            console.log(error.responseJSON);
    })

}

function leerdatos(id){
    const ruta="../controladoresadmin/restaurante.leerdatos.php";
    
    $.post(
        ruta,
        {
            p_codigo_restaurante:id
        }
        

    ).done(function(resultado){

        if(resultado.estado==200){
            
            
            const datosJSON=resultado.datos;
            $('#txtCodigoRestaurante').val(datosJSON.id);
            $('#txtNombreRestaurante').val(datosJSON.nombre);
            $('#txtDireccionRestaurante').val(datosJSON.direccion);
            $('#txtTelefonoRestaurante').val(datosJSON.telefono);
            $('#txtCapacidadRestaurante').val(datosJSON.capacidad);
            $('#txtDescripcionRestaurante').val(datosJSON.descripcion);

            $('#cboUsuario').val(datosJSON.codigo_usuario);
            $('#cboUsuario').change();

            $('#cboTipoComida').val(datosJSON.idtipocomida);
            $('#cboTipoComida').change();
            
            
            




            $('#operacion').val('editar');
            $('#modal-restaurante').modal("show");
        }else{
             console.log(resultado);
        }
        
    }).fail(function(error){
            console.log(error.responseJSON);
    })
}
// function cambiar(){
//     let pdrs = document.getElementById('imgRestaurante').files[0].name;
//     document.getElementById('info').innerHTML = pdrs;
// }