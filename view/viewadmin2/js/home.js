$(document).ready(function(){
    $('#txtName').val(window.localStorage.getItem('fullname'));
    $('#txtEmail').val(window.localStorage.getItem('email'));
    $('#txtPassActual').val("");
    $('#txtPassNew').val("");
    $('#txtPassNew2').val("");


    gDataInit();
});


// editar datos del ususario

$('#frmUpdateUser').submit(function(e){
    e.preventDefault();

    const ruta ='../controladores/usuario.editar.php';
    
    let name=$('#txtName').val();
    let email=$('#txtEmail').val();
    let passactual=$('#txtPassActual').val();
    let passnew=$('#txtPassNew').val();
    let passnew2=$('#txtPassNew2').val();

    $.post(
        ruta,
        {
             p_id:window.localStorage.getItem('idUsuario'),
             p_nombre:name,
             p_email:email,
             p_contrasena:passactual,
             p_contrasena_nueva:passnew,
             p_contrasena_nueva_2:passnew2

        }

    ).done(function(resultado){

        if(resultado.estado==200){
            //  var datosJSON=resultado.datos;
            console.log(resultado);

            window.localStorage.setItem('fullname',name);
            window.localStorage.setItem('email',email);
            
            // $('#txtName').val(window.localStorage.getItem('fullname'));
            // $('#txtEmail').val(window.localStorage.getItem('email'));

            // $('#txtPassActual').val("");
            // $('#txtPassNew').val("");
            // $('#txtPassNew2').val("");
            location.reload();
             
        }else{
             console.log(resultado);
        }
        
        

        // location.href='home.php';
    }).fail(function(error){
            console.log(error.responseJSON);
    })
     
})

function gDataInit(){
    const ruta ='../controladores/datosglobales.home.php';
    $.post(
        ruta,
        

    ).done(function(resultado){

        if(resultado.estado==200){
            
            // console.log(resultado);
            const datosJSON=resultado.datos;

            $('#num_usuarios').html(datosJSON.c_usuarios);
            $('#num_restaurantes').html(datosJSON.c_restaurantes);
            $('#num_reservas').html(datosJSON.c_reservas);
            
             
        }else{
             console.log(resultado);
        }
        
        

        
    }).fail(function(error){
            console.log(error.responseJSON);
    })
}