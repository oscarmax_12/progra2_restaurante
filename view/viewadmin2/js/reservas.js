$(document).ready(function(){
    
    // listar();
    cargarComboUsuario("#cboUsuarioReserva","seleccione");
    cargarComboRestaurante("#cboRestauranteReserva","seleccione")
  
})
function listar(){
    const ruta="../controladores/reserva.listar.php";

    $.post(
        ruta
        

    ).done(function(resultado){

        if(resultado.estado==200){
            
            console.log(resultado);
            let datosJSON=resultado.datos;

            let html='';
            
            $.each(datosJSON, function (i, item) {
               
                html += '<tr>';
                html += '<td >' + item.id + '</td>';
                html += '<td >' + item.fecha + '</td>';
                html += '<td >' + item.hora + '</td>';
                html += '<td >' + item.cantidadpersonas + '</td>';
                html += '<td >' + item.usuario + '</td>';
                html += '<td >' + item.restaurante + '</td>';
                html += '<td >' + item.estado + '</td>';
                
                
               
                
                html += '<td><button class="btn btn-danger" onclick="eliminar('+item.id+')">Eliminar</button> <button class="btn btn-primary" onclick="leerdatos('+item.id+')">Editar</button></td>'; 
                
                

            //     html += '</tr>';
            });
            $('#tblReservas').html(html);
            $("#example1").dataTable();
          

            
             
        }else{
             console.log(resultado);
        }
        
        

        
    }).fail(function(error){
            console.log(error.responseJSON);
    })
}


function eliminar(id){
    const ruta = "../controladores/reserva.eliminar.php";
    $.post(
        ruta,
        {
            p_codigo_reserva:id
        }
        

    ).done(function(resultado){

        if(resultado.estado==200){
            
            listar();
        }else{
             console.log(resultado);
        }
        
    }).fail(function(error){
            console.log(error.responseJSON);
    })
}

$('#btnagregarRva').on('click',function(e){
    e.preventDefault();
    
    $('#txtCodigoReserva').val("");
    $('#txtFechaReserva').val("");
    $('#txtHoraReserva').val("");
    $('#txtCantidadPersonas').val("");
    $('#txtEstado').val("");
    

    $('#operacion').val('agregar');
    $('#modal-reserva').modal("show");
})
$('#btnGuardarReserva').on('click',function(){
    if($('#operacion').val()=="agregar"){
        agregarReserva();
        $('#modal-reserva').modal("hide");
        $('#txtCodigoReserva').val("");
        $('#txtFechaReserva').val("");
        $('#txtHoraReserva').val("");
        $('#txtCantidadPersonas').val("");
        $('#txtEstado').val("");
        

        $('#operacion').val('');
        listar();
    }else{

        editarReserva();
        $('#modal-reserva').modal("hide");
        
        $('#txtCodigoReserva').val("");
        $('#txtFechaReserva').val("");
        $('#txtHoraReserva').val("");
        $('#txtCantidadPersonas').val("");
        $('#txtEstado').val("");


        $('#operacion').val('');
        
    }
})

function agregarReserva(){
    const ruta="../controladores/reserva.agregar.php";
    let fecha =$('#txtFechaReserva').val();
    let hora=$('#txtHoraReserva').val();
    let cantidad_personas=$('#txtCantidadPersonas').val();
    let usuario=$('#cboUsuarioReserva').val();
    let restaurante=$('#cboRestauranteReserva').val();
    let estado=$('#txtEstado').val();
    
    
    
    $.post(
        ruta,
        {
            p_fecha:fecha,
            p_hora:hora,
            p_cantidadpersonas:cantidad_personas,
            p_usuario:usuario,
            p_restaurante:restaurante,
            p_estado:estado

        }
        

    ).done(function(resultado){

        if(resultado.estado==200){
            
            console.log(resultado);
            
        }else{
             console.log(resultado);
        }
        
    }).fail(function(error){
            console.log(error.responseJSON);
    })

}

function editarReserva(){
    const ruta="../controladores/reserva.editar.php";

    let codigo_res=$('#txtCodigoReserva').val();
    let fecha_res =$('#txtFechaReserva').val();
    let hora_res=$('#txtHoraReserva').val();
    let cantpersonas_res=$('#txtCantidadPersonas').val();
    let usuario_res=$('#cboUsuarioReserva').val();
    let restaurante_res=$('#cboRestauranteReserva').val();
    let estado_res=$('#txtEstado').val();


    


    $.post(
        ruta,
        {
            p_codigo:codigo_res,

            p_fecha:fecha_res,
            p_hora:hora_res,
            p_cantpersonas:cantpersonas_res,
            p_usuario:usuario_res,
            p_restaurante:restaurante_res,
            p_estado:estado_res
            

        }
        

    ).done(function(resultado){

        if(resultado.estado==200){
            
            listar();
            
        }else{
             console.log(resultado);
        }
        
    }).fail(function(error){
            console.log(error.responseJSON);
    })
}

function leerdatos(id){
    const ruta="../controladores/reserva.leerdatos.php";
    
    $.post(
        ruta,
        {
            p_codigo_reserva:id
        }
        

    ).done(function(resultado){

        if(resultado.estado==200){
            
            
            const datosJSON=resultado.datos;
            $('#txtCodigoReserva').val(datosJSON.id);
            $('#txtFechaReserva').val(datosJSON.fecha);
            $('#txtHoraReserva').val(datosJSON.hora);
            $('#txtCantidadPersonas').val(datosJSON.cantidadpersonas);
            $('#cboUsuarioReserva').val(datosJSON.idusuario);
            $('#cboRestauranteReserva').val(datosJSON.idrestaurante);
            $('#txtEstado').val(datosJSON.estado);
            




            $('#operacion').val('editar');
            $('#modal-reserva').modal("show");
        }else{
             console.log(resultado);
        }
        
    }).fail(function(error){
            console.log(error.responseJSON);
    })
}