<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SysAdmin | Home</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bootstrap_tools/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bootstrap_tools/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bootstrap_tools/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="bootstrap_tools/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="bootstrap_tools/dist/css/skins/_all-skins.min.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link rel="stylesheet" href="css/styles.css">

  
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">


     <!-- header section -->
     <?php include_once 'header.php'?>
     <!-- header section -->

     <!-- menu section -->
     <?php include_once 'sidebar_menu.php'?>
     <!-- menu section -->

  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- header of the page -->
    <section class="content-header">
      <h1>
        Incio
        <small>Estás en la página de inicio</small>
      </h1>
      
    </section>
    <!-- header of the page -->

    <!-- Main content -->
    <section class="content">

      <!-- global data box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">DATOS GLOBALES</h3>

          
        </div>
        <div class="box-body">
          <!-- write the content of page -->
          <div class="row">
              <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-aqua">
                  <div class="inner"></div>
                    <h3 class="text-center" id="num_usuarios">120</h3>
                    <p class="text-center">Usuarios</p>
                </div>

              </div>
              <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-green">
                  <div class="inner"></div>
                    <h3 class="text-center" id="num_restaurantes">130</h3>
                    <p class="text-center">Restaurantes</p>
                </div>

              </div>

              <div class="col-lg-4 col-xs-12">
                <div class="small-box bg-yellow">
                  <div class="inner"></div>
                    <h3 class="text-center" id="num_reservas">130</h3>
                    <p class="text-center">Reservas</p>
                </div>

              </div>
          </div>


        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Esta sección contiene datos actuales.
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- global data box -->

      <!-- data profile -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">MI PERFIL</h3>

          
        </div>
        <div class="box-body">
          <!-- write the content of page -->
            <div class="row">
              <form role="form" id="frmUpdateUser">
              <div class="col-lg-6">
                <img src="img/user-profile.png" alt="Image profile" class="img-md">
                <input type="file" class="btn btn-default">
              </div>

              <div class="col-lg-6">
                <div class="form-group">
                  <label for="">Nombre completo </label>
                  <input type="text" class="form-control" value="Daniel Chanduvi" id="txtName">
                </div>
                <div class="form-group">
                  <label for="">Correo electrónico </label>
                  <input type="email" class="form-control" value="dchanduvisiesquen@gmail.com" id="txtEmail" >
                </div>
                <div class="form-group">
                  <label for="">Contraseña actual</label>
                  <input type="password" class="form-control" value="" id="txtPassActual">
                </div>
                <div class="form-group">
                  <label for="">Contraseña nueva</label>
                  <input type="password" class="form-control" value="" id="txtPassNew">
                </div>
                <div class="form-group">
                  <label for="">Confirma nueva contraseña</label>
                  <input type="password" class="form-control" value="" id="txtPassNew2">
                </div>
                <input type="submit" value="Actualizar datos" class="btn btn-success btn-block" id="btnUpdateUser">
              </div>

              

              </form>
              
            </div>
          <!-- write the content of page -->
          


        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Esta sección contiene datos actuales del perfil del usuario
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- data profile -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

     <!-- footer section -->
     <?php include_once 'footer.php';?>
     <!-- footer section -->

  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bootstrap_tools/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bootstrap_tools/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="bootstrap_tools/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bootstrap_tools/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="bootstrap_tools/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<!-- <script src="../../dist/js/demo.js"></script> -->
<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>
<script src="js/header.js"></script>
<script src="js/sidebar_menu.js"></script>
<script src="js/home.js"></script>
</body>
</html>
