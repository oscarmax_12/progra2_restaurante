<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SysAdmin | Tipo Comida</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bootstrap_tools/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bootstrap_tools/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bootstrap_tools/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="bootstrap_tools/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="bootstrap_tools/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="bootstrap_tools/dist/css/skins/_all-skins.min.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link rel="stylesheet" href="css/styles.css">

  
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">


     <!-- header section -->
     <?php include_once 'header.php'?>
     <!-- header section -->

     <!-- menu section -->
     <?php include_once 'sidebar_menu.php'?>
     <!-- menu section -->

  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- header of the page -->
    <section class="content-header">
      <h1>
        Tipo comida
        
      </h1>
      
    </section>
    <!-- header of the page -->

    <!-- Main content -->
    <section class="content">

      <!-- global data box -->
      <div class="box">
          <div class="box-header">
              <h3 class="box-title">Tipos de comida registrados </h3>
              <br>
              <button type="button" class="btn btn-success btn-new" id="btnagregarTC">
                Nuevo tipo de comida
              </button>
          </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tbl" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Nombre</th>
                  
                  <th>Acciones</th>
                </tr>
                </thead>
                <tbody id="tblTipoComida">
                <!-- <tr>
                  <td>1</td>
                  <td>Peruana</td>
                  
                  <td>
                    <button class="btn btn-danger" >Eliminar</button>
                    <button class="btn btn-primary" data-toggle="modal" data-target="#modal-tipocomida">Editar</button>
                  </td>
                </tr> -->
                
                
                </tbody>
                <tfoot>
                <tr>
                  <th>Id</th>
                  <th>Nombre</th>
                  
                  <th>Acciones</th>
                </tr>
                </tfoot>
              </table>
            </div>
        <!-- /.box-footer-->
        <div class="modal fade" id="modal-tipocomida">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tipo de comida</h4>
              </div>
              <div class="modal-body">
                    <div class="row">
                      <div class="col-xs-6">
                        <div class="form-group">
                          <input type="text" id="operacion" hidden>
                          <label for="">Codigo de tipo de comida</label>
                          <input type="text" class="form-control" id="txtCodigoComida" readonly>
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <div class="form-group">
                          
                          <label for="">Nombre de tipo de comida</label>
                          <input type="text" class="form-control" id="txtComida">

                        </div>
                          
                      </div>
                         
                         
                    </div>
                
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btnGuardarTipoComida">Save changes</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
      </div>
      <!-- global data box -->

     

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

     <!-- footer section -->
     <?php include_once 'footer.php';?>
     <!-- footer section -->

  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bootstrap_tools/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bootstrap_tools/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="bootstrap_tools/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="bootstrap_tools/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="bootstrap_tools/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bootstrap_tools/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="bootstrap_tools/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="bootstrap_tools/dist/js/demo.js"></script>
<!-- page script -->

<script src="js/header.js"></script>
<script src="js/sidebar_menu.js"></script>
<script src="js/tipocomida.js"></script>
</body>
</html>
