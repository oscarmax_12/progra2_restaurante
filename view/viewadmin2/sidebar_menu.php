<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="img/user-profile.png" class="img-circle" alt="User Image" class="img-sr">
        </div>
        <div class="pull-left info">
          <p id="user-name-three">Daniel Alonso</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li><a href="home.php"><i class="glyphicon glyphicon-user"></i> Mi perfil</a></li>
        <li><a href="usuarios.php"><i class="fa fa-circle-o"></i> Usuarios</a></li>
        <li><a href="tipocomida.php"><i class="glyphicon glyphicon-cutlery"></i> Tipo comida</a></li>
        <li><a href="restaurante.php"><i class="glyphicon glyphicon-glass"></i> Restaurantes</a></li>
        <li><a href="reservas.php"><i class="glyphicon glyphicon-saved"></i> Reservas</a></li>
        
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>