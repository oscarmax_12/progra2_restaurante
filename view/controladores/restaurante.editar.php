<?php
header('Access-Control-Allow-Origin: *');
require_once '../conexion/Conexion.clase.php';
require_once '../librerias/lib.php';



try {
    if(
        empty($_POST['p_codigo'])||
        empty($_POST['p_nombre'])||
        empty($_POST['p_direccion'])||
        empty($_POST['p_telefono'])||
        empty($_POST['p_tipocomida'])||
        empty($_POST['p_capacidad'])||
        empty($_POST['p_descripcion'])||
        empty($_POST['p_imagen'])||
        empty($_POST['p_usuario'])
    ){
        Funciones::imprimeJSON(500,"FALTAN DATOS PARA REALIZAR LA OPERACIÓN","");    
        exit();
    }

    $codigo=$_POST['p_codigo'];
    
    $nombre=$_POST['p_nombre'];
    $direccion=$_POST['p_direccion'];
    $telefono=$_POST['p_telefono'];
    $tipocomida=$_POST['p_tipocomida'];
    $capacidad=$_POST['p_capacidad'];
    $descripcion=$_POST['p_descripcion'];
    $imagen=$_POST['p_imagen'];
    $usuario=$_POST['p_usuario'];

    $sql="update restaurante
    set
    
        nombre = '".$nombre."',
        direccion = '".$direccion."',
        telefono = '".$telefono."',
        capacidad = '".$capacidad."',
        descripcion = '".$descripcion."',
        idtipocomida = '".$tipocomida."',
        imagen = '".$imagen."',
        tipo_usuario = 1,
        usuario ='".$usuario."',
    where id = '".$codigo."';";
    $confirmacion=0;
    $cnx->query($sql) or $confirmacion=1;
    if($confirmacion==0){
        Funciones::imprimeJSON(200,"Exito al editar el tipo de comida","");
    }else{
        Funciones::imprimeJSON(500,"Error al editar el tipo de comida","");
    }
} catch (Exception $e) {
    Funciones::imprimeJSON(500,$e->getMessage(),"");
}
?>