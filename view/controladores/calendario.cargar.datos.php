<?php
header('Access-Control-Allow-Origin: *');
require_once '../conexion/Conexion.clase.php';
require_once '../librerias/lib.php';


    try {
        
        if (
            empty($_GET['id'])
    ) {
        Funciones::imprimeJSON(500, "LOS DATOS NO ESTAN COMPLETOS", "");
    }

    $codigo_usuario = $_GET['id'];
    
    
    $sql2 = "
         SELECT r.id FROM   restaurante r
inner JOIN usuarios u ON r.codigo_usuario=u.codigo_usuario
WHERE u.codigo_usuario=".$codigo_usuario;
            $sentencia =$cnx->prepare($sql2);
            $sentencia->execute();
            if ($sentencia->rowCount()) {
                $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
                $id_rest= $resultado["id"];
            } else {
                throw new Exception("No se ha encontrado al restaurante ");
            }
    
    
    
        
        
        $sql="
        SELECT CONCAT(r.fecha,' ',r.hora) AS 'start',
        CONCAT(r.fecha,' ',r.hora) AS 'end',
        '#FFFFFF' AS textcolor,
        '##FF0F0' AS color,
        CONCAT(p.apellido_paterno,' ',p.apellido_materno,', ',p.nombres) AS title,
        r.idusuario	  			
        FROM reserva r
        INNER JOIN usuarios u ON  r.idusuario=u.codigo_usuario
        INNER JOIN detalle_usuario_persona dt ON u.codigo_usuario=dt.codigo_usuario
        INNER JOIN persona p ON dt.codigo_persona=p.codigo_persona
        WHERE r.idrestaurante=".$id_rest;

        $result = $cnx->query($sql);
        $respuesta=$result->fetchAll(PDO::FETCH_ASSOC);
        if($respuesta){
            echo json_encode($respuesta);
        }else{
            echo json_encode('error');
        }
        
    } catch (Exception $e) {
        Funciones::imprimeJSON(500,$e->getMessage(),"");
    }
?>