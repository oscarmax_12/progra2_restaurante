<?php
header('Access-Control-Allow-Origin: *');
require_once '../conexion/Conexion.clase.php';
require_once '../librerias/lib.php';

try {
    
    if(
        empty($_POST['p_codigo_restaurante'])
    ){
        Funciones::imprimeJSON(500,"FALTAN DATOS PARA ELIMINAR","");
        exit();
    }

    $id_restaurante=$_POST['p_codigo_restaurante'];
    $sql= "delete from restaurante  where id='".$id_restaurante."';";

    $confirmacion=0;
    $cnx->query($sql) or $confirmacion=1;

    if($confirmacion==0){
        Funciones::imprimeJSON(200,"Exito al eliminar","");
    }else{
        Funciones::imprimeJSON(500,"Error al eliminar","");
    }


} catch (Exception $e) {
    Funciones::imprimeJSON(500,$e->getMessage(),"");
}

?>