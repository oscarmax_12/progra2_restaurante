<?php
header('Access-Control-Allow-Origin: *');
require_once '../conexion/Conexion.clase.php';
require_once '../librerias/lib.php';


    try {
        $sql="select * from tipocomida;";
        $result = $cnx->query($sql);
        $respuesta=$result->fetchAll(PDO::FETCH_ASSOC);
        if($respuesta){
            Funciones::imprimeJSON(200,"lista de tipo de comida",$respuesta);
        }else{
            Funciones::imprimeJSON(500,"Error al listar","");
        }
        
    } catch (Exception $e) {
        Funciones::imprimeJSON(500,$e->getMessage(),"");
    }
?>