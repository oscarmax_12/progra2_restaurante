<?php
header('Access-Control-Allow-Origin: *');
require_once '../conexion/Conexion.clase.php';
require_once '../librerias/lib.php';



try {
    if(
        empty($_POST['p_comida'])
    ){
        Funciones::imprimeJSON(500,"FALTAN DATOS PARA REALIZAR LA OPERACIÓN","");    
        exit();
    }

    $comida=$_POST['p_comida'];

    $sql="insert into tipocomida (descripcion) VALUES ('".$comida."');";
    $confirmacion=0;
    $cnx->query($sql) or $confirmacion=1;
    if($confirmacion==0){
        Funciones::imprimeJSON(200,"Exito al agregar el tipo de comida","");
    }else{
        Funciones::imprimeJSON(500,"Error al agregar el tipo de comida","");
    }
} catch (Exception $e) {
    Funciones::imprimeJSON(500,$e->getMessage(),"");
}
?>