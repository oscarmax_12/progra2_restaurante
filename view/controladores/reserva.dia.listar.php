<?php

header('Access-Control-Allow-Origin: *');
require_once '../conexion/Conexion.clase.php';
require_once '../librerias/lib.php';


try {

    if (
            empty($_POST['p_codigo_usuario'])
    ) {
        Funciones::imprimeJSON(500, "LOS DATOS NO ESTAN COMPLETOS", "");
    }

    $codigo_usuario = $_POST['p_codigo_usuario'];
    $sql = "
        SELECT c.id,DATE_FORMAT(c.start, '%Y- %m - %d') AS dia,c.cantidad FROM calendario c
        INNER JOIN restaurante r ON c.id_restaurante=r.id
        INNER JOIN usuarios u ON r.codigo_usuario=u.codigo_usuario
        WHERE u.codigo_usuario=" . $codigo_usuario . ";";
    $result = $cnx->query($sql);
    $respuesta = $result->fetchAll(PDO::FETCH_ASSOC);
    if ($respuesta) {
        Funciones::imprimeJSON(200, "lista de usuarios", $respuesta);
    } else {
        Funciones::imprimeJSON(500, "Error al listar", "");
    }
} catch (Exception $e) {
    Funciones::imprimeJSON(500, $e->getMessage(), "");
}
?>