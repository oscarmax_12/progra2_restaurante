<?php
header('Access-Control-Allow-Origin: *');
require_once '../conexion/Conexion.clase.php';
require_once '../librerias/lib.php';


    try {
        $sql="select 
        r.id,
        r.nombre,
        r.direccion,
        r.telefono,
        r.capacidad,
        r.descripcion,
        tc.descripcion  AS comida,
        r.imagen,
        r.tipo_usuario,
        r.usuario,
        r.clave
        
        from restaurante r 
            inner join tipocomida tc on (r.idtipocomida=tc.id);";
        $result = $cnx->query($sql);
        $respuesta=$result->fetchAll(PDO::FETCH_ASSOC);
        if($respuesta){
            Funciones::imprimeJSON(200,"lista de restaurante",$respuesta);
        }else{
            Funciones::imprimeJSON(500,"Error al listar","");
        }
        
    } catch (Exception $e) {
        Funciones::imprimeJSON(500,$e->getMessage(),"");
    }
?>