<?php
header('Access-Control-Allow-Origin: *');
require_once '../conexion/Conexion.clase.php';
require_once '../librerias/lib.php';



try {
    if(
        empty($_POST['p_fecha'])||
        empty($_POST['p_descripcion'])||
        empty($_POST['p_color'])||
        empty($_POST['p_text'])||
        empty($_POST['p_id_usuario'])||
        empty($_POST['p_cantidad'])
    ){
        Funciones::imprimeJSON(500,"FALTAN DATOS PARA REALIZAR LA OPERACIÓN","");    
        exit();
    }

    $fecha=$_POST['p_fecha'];
    $desc=$_POST['p_descripcion'];
    $colors= $_POST['p_color'];
    $text=$_POST['p_text'];
    $user=$_POST['p_id_usuario'];
    $cantidad=$_POST['p_cantidad'];
    
    $id_rest=null;
    
    
     $sql2 = "
         SELECT r.id FROM   restaurante r
inner JOIN usuarios u ON r.codigo_usuario=u.codigo_usuario
WHERE u.codigo_usuario=".$user;
            $sentencia =$cnx->prepare($sql2);
            $sentencia->execute();
            if ($sentencia->rowCount()) {
                $resultado = $sentencia->fetch(PDO::FETCH_ASSOC);
                $id_rest= $resultado["id"];
            } else {
                throw new Exception("No se ha encontrado al restaurante ");
            }
    
    
    
    



    $sql="insert into calendario
    (   title,
        descripcion,
        color,
        textcolor,
        start,
        end,
        id_restaurante,
        cantidad
    )
    values
    ('".$desc."',
        '".$desc."',
        '".$colors."',
        '".$text."',
        '".$fecha."',
        '".$fecha."',
        ".$id_rest.",
        ".$cantidad."
    );";

    echo $sql;
    
    $confirmacion=0;
    $cnx->query($sql) or $confirmacion=1;
    if($confirmacion==0){
        Funciones::imprimeJSON(200,"Exito al agregar el registro","");
    }else{
        Funciones::imprimeJSON(500,"Error al agregar el restaurante","");
    }
} catch (Exception $e) {
    Funciones::imprimeJSON(500,$e->getMessage(),"");
}



?>