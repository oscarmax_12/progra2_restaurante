<?php
header('Access-Control-Allow-Origin: *');
require_once '../conexion/Conexion.clase.php';
require_once '../librerias/lib.php';


    try {

        if(
            empty($_POST['p_codigo_tipo_comida'])
        ){
            Funciones::imprimeJSON(500,"LOS DATOS NO ESTAN COMPLETOS","");
        }

        $codigo_tipo_comida=$_POST['p_codigo_tipo_comida'];

        $sql="select * from tipocomida where id='".$codigo_tipo_comida."' ;";
        $result = $cnx->query($sql);
        $respuesta=$result->fetch(PDO::FETCH_ASSOC);
        if($respuesta){
            Funciones::imprimeJSON(200,"lista de tipo de comida",$respuesta);
        }else{
            Funciones::imprimeJSON(500,"Error al listar","");
        }
        
    } catch (Exception $e) {
        Funciones::imprimeJSON(500,$e->getMessage(),"");
    }
?>