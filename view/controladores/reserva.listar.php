<?php
header('Access-Control-Allow-Origin: *');
require_once '../conexion/Conexion.clase.php';
require_once '../librerias/lib.php';


    try {
        $sql="
            select 
                r.id as id, 
                r.fecha as fecha, 
                r.hora as hora, 
                r.cantidadpersonas as cantidadpersonas, 
                u.nombre as usuario, 
                rs.nombre as restaurante, 
                r.estado as estado
            from reserva r
                inner join usuarios u on (r.idusuario=u.id)
                inner join restaurante rs on (r.idrestaurante=rs.id);";
        $result = $cnx->query($sql);
        $respuesta=$result->fetchAll(PDO::FETCH_ASSOC);
        if($respuesta){
            Funciones::imprimeJSON(200,"lista de reservas",$respuesta);
        }else{
            Funciones::imprimeJSON(500,"Error al listar","");
        }
        
    } catch (Exception $e) {
        Funciones::imprimeJSON(500,$e->getMessage(),"");
    }
?>