<?php
header('Access-Control-Allow-Origin: *');
require_once '../conexion/Conexion.clase.php';
require_once '../librerias/lib.php';


    try {

        if(
            empty($_POST['p_codigo_usuario'])
        ){
            Funciones::imprimeJSON(500,"LOS DATOS NO ESTAN COMPLETOS","");
        }

        $codigo_usuario=$_POST['p_codigo_usuario'];

        $sql="select id, nombre, apellido, email, tipo_usuario, foto, estado from usuarios where id= '".$codigo_usuario."';";
        $result = $cnx->query($sql);
        $respuesta=$result->fetch(PDO::FETCH_ASSOC);
        if($respuesta){
            Funciones::imprimeJSON(200,"datos de usuario",$respuesta);
        }else{
            Funciones::imprimeJSON(500,"Error al listar","");
        }
        
    } catch (Exception $e) {
        Funciones::imprimeJSON(500,$e->getMessage(),"");
    }
?>