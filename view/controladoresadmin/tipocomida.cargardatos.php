<?php
header('Access-Control-Allow-Origin: *');
require_once '../conexion/Conexion.clase.php';
require_once '../librerias/lib.php';


    try {
        $sql="select * from tipocomida;";
        $result = $cnx->query($sql);
        $respuesta=$result->fetchAll(PDO::FETCH_ASSOC);
        if($respuesta){
            Funciones::imprimeJSON(200,"Los datos están cargados - tipo comida",$respuesta);
        }else{
            Funciones::imprimeJSON(500,"Error al cargar los datos","");
        }
        
    } catch (Exception $e) {
        Funciones::imprimeJSON(500,$e->getMessage(),"");
    }
?>