<?php
header('Access-Control-Allow-Origin: *');
require_once '../conexion/Conexion.clase.php';
require_once '../librerias/lib.php';


    try {
        $sql="
            SELECT u.codigo_usuario AS 'id', u.email AS 'nombre' from usuarios u 
        LEFT JOIN restaurante r ON u.codigo_usuario=r.codigo_usuario
        WHERE  u.codigo_usuario!=53
        ";
        $result = $cnx->query($sql);
        $respuesta=$result->fetchAll(PDO::FETCH_ASSOC);
        if($respuesta){
            
            Funciones::imprimeJSON(200,"Los datos están cargados - reserva",$respuesta);
        }else{
            Funciones::imprimeJSON(500,"Error al cargar los datos","");
        }
        
    } catch (Exception $e) {
        Funciones::imprimeJSON(500,$e->getMessage(),"");
    }
?>