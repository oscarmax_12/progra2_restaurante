<?php
header('Access-Control-Allow-Origin: *');
require_once '../conexion/Conexion.clase.php';
require_once '../librerias/lib.php';


    try {
        $sql="
        SELECT r.id,
        r.nombre,
        r.direccion,
        r.telefono,
        r.capacidad,
        r.descripcion,
        tc.descripcion  AS comida,
        r.imagen,
        (case 
                  when r.tipo_usuario=1 THEN 'USER RESTAURANTE'
                WHEN r.tipo_usuario=2 THEN 'USER ADMIN'
                ELSE 'USER CLIENTE'
                END ) AS tipo_usuario,
                u.email AS usuario
 
 FROM restaurante r
 INNER JOIN usuarios u ON r.codigo_usuario=u.codigo_usuario
 INNER JOIN detalle_usuario_persona dp ON u.codigo_usuario=dp.codigo_usuario
 INNER JOIN persona p ON dp.codigo_persona=p.codigo_persona
 inner join tipocomida tc on r.idtipocomida=tc.id;";
        $result = $cnx->query($sql);
        $respuesta=$result->fetchAll(PDO::FETCH_ASSOC);
        if($respuesta){
            Funciones::imprimeJSON(200,"lista de restaurante",$respuesta);
        }else{
            Funciones::imprimeJSON(500,"Error al listar","");
        }
        
    } catch (Exception $e) {
        Funciones::imprimeJSON(500,$e->getMessage(),"");
    }
?>