<?php
header('Access-Control-Allow-Origin: *');
require_once '../conexion/Conexion.clase.php';
require_once '../librerias/lib.php';


    try {
        $sql="
        SELECT u.codigo_usuario as id,
        p.nombres AS nombre,
        CONCAT(p.apellido_paterno,' ',p.apellido_materno)AS apellido,
        u.email,
        (case u.tipo_usuario
		  when 1 then 'RESTAURANTE' 
		  when 2 then 'ADMINISTRADOR' 
		  when 3 then 'CLIENTE'
		  END)AS tipo_usuario,
        u.foto,
        (case u.estado
		  when 1 THEN 'ACTIVO'
		  WHEN 2 THEN 'INACTIVO'
		  END)AS estado
FROM usuarios u
INNER JOIN detalle_usuario_persona dp ON u.codigo_usuario=dp.codigo_usuario
INNER JOIN persona p ON dp.codigo_persona=p.codigo_persona
        ";
        $result = $cnx->query($sql);
        $respuesta=$result->fetchAll(PDO::FETCH_ASSOC);
        if($respuesta){
            Funciones::imprimeJSON(200,"lista de usuarios",$respuesta);
        }else{
            Funciones::imprimeJSON(500,"Error al listar","");
        }
        
    } catch (Exception $e) {
        Funciones::imprimeJSON(500,$e->getMessage(),"");
    }
?>