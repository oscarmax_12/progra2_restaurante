<?php

header('Access-Control-Allow-Origin: *');
require_once '../conexion/Conexion.clase.php';
require_once '../librerias/lib.php';
require_once '../../viewprincipalusuario/webprincipal/img_restaurantes/';



try {
    if (
            empty($_POST["txtNombreRestaurante"]) ||
            empty($_POST["txtDireccionRestaurante"]) ||
            empty($_POST["txtTelefonoRestaurante"]) ||
            empty($_POST["txtCapacidadRestaurante"]) ||
            empty($_POST["txtDescripcionRestaurante"]) ||
            empty($_POST["cboTipoComida"]) ||
            empty($_POST["cboUsuario"])
    ) {
        Funciones::imprimeJSON(500, "FALTAN DATOS PARA REALIZAR LA OPERACIÓN", "");
        exit();
    }
    $imagen = $_FILES["imgRestaurante"];
    $nombre = $_POST["txtNombreRestaurante"];
    $direccion = $_POST["txtDireccionRestaurante"];
    $telef = $_POST["txtTelefonoRestaurante"];
    $capacidad = $_POST["txtCapacidadRestaurante"];
    $descripo = $_POST["txtDescripcionRestaurante"];
    $tipocomida = $_POST["cboTipoComida"];
    $cod_user = $_POST["cboUsuario"];





    $sql = "insert into restaurante
    (
        nombre,
        direccion,
        telefono,
        capacidad,
        descripcion,
        idtipocomida,
        imagen,
        tipo_usuario,
        codigo_usuario,
    )
    values
    (
        '" . $nombre . "',
        '" . $direccion . "',
        '" . $telef . "',
        '" . $capacidad . "',
        '" . $descripo . "',
        '" . $tipocomida . "',
        'imagen-insert',
        1,
        '" . $cod_user . "'
    );";


    $confirmacion = 0;
    $cnx->query($sql) or $confirmacion = 1;
    if ($confirmacion == 0) {
        //REGISTRO DE LA FOTO

        $sql_persona = " SELECT MAX(id)AS id FROM restaurante";
        $result = $cnx->query($sql_persona);
        $respuesta_persona = $result->fetch(PDO::FETCH_ASSOC);
        $id_rest = $respuesta_persona['id'];



        $nombre_foto = '';

        if ($imagen["type"] == "image/jpg") {
            $nombre_foto = $id_rest . ".jpg";
        }
        if ($imagen["type"] == "image/jpeg") {
            $nombre_foto = $id_rest . ".jpeg";
        }
        if ($imagen["type"] == "image/png") {
            $nombre_foto = $id_rest . ".png";
        }
        if ($imagen["type"] == "") {
            echo 'el formato del archivo no es válido';
            exit();
        }
        $ruta_guardar = "../../viewprincipalusuario/webprincipal/img_restaurantes/" . $nombre_foto;
        move_uploaded_file($imagen["tmp_name"], $ruta_guardar);
        echo 'success';
    } else {
        Funciones::imprimeJSON(500, "Error al agregar el restaurante", "");
    }
} catch (Exception $e) {
    Funciones::imprimeJSON(500, $e->getMessage(), "");
}
?>
