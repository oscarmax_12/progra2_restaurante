<?php
header('Access-Control-Allow-Origin: *');
require_once '../conexion/Conexion.clase.php';
require_once '../librerias/lib.php';


    try {

        if(
            empty($_POST['p_codigo_reserva'])
        ){
            Funciones::imprimeJSON(500,"LOS DATOS NO ESTAN COMPLETOS","");
        }

        $codigo_reserva=$_POST['p_codigo_reserva'];

        $sql="select * from reserva where id='".$codigo_reserva."';";
        $result = $cnx->query($sql);
        $respuesta=$result->fetch(PDO::FETCH_ASSOC);
        if($respuesta){
            Funciones::imprimeJSON(200,"Datos de la reserva",$respuesta);
        }else{
            Funciones::imprimeJSON(500,"Error al mostrar los datos","");
        }
        
    } catch (Exception $e) {
        Funciones::imprimeJSON(500,$e->getMessage(),"");
    }
?>