<?php
header('Access-Control-Allow-Origin: *');
require_once '../conexion/Conexion.clase.php';
require_once '../librerias/lib.php';


    try {

        if(
            empty($_POST['p_codigo_restaurante'])
        ){
            Funciones::imprimeJSON(500,"LOS DATOS NO ESTAN COMPLETOS","");
        }

        $codigo_restaurante=$_POST['p_codigo_restaurante'];

        $sql="SELECT r.id, r.nombre, r.direccion, r.telefono,r.capacidad, r.descripcion, r.idtipocomida, r.imagen,
                u.codigo_usuario
                   from restaurante r
                    INNER JOIN usuarios u ON r.codigo_usuario=u.codigo_usuario where r.id='".$codigo_restaurante."' ;";
        $result = $cnx->query($sql);
        $respuesta=$result->fetch(PDO::FETCH_ASSOC);
        if($respuesta){
            Funciones::imprimeJSON(200,"Datos del restaurante",$respuesta);
        }else{
            Funciones::imprimeJSON(500,"Error al mostrar los datos","");
        }
        
    } catch (Exception $e) {
        Funciones::imprimeJSON(500,$e->getMessage(),"");
    }
?>