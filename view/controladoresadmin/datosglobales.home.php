<?php
header('Access-Control-Allow-Origin: *');
require_once '../conexion/Conexion.clase.php';
require_once '../librerias/lib.php';

try {
    // cantidad de usuarios
    $sql="select count(*) as cantidad_usuarios from usuarios";
    $result = $cnx->query($sql);
    $respuesta=$result->fetch(PDO::FETCH_ASSOC);
    
    if($respuesta){
        $cantidad_usuarios=$respuesta['cantidad_usuarios'];
        // Funciones::imprimeJSON(200,"",$respuesta['cantidad_usuarios']);
    }
    // cantidad de usuarios

    // cantidad de restaurantes
    $sql="select count(*) as cantidad_restaurantes from restaurante;";
    $result = $cnx->query($sql);
    $respuesta=$result->fetch(PDO::FETCH_ASSOC);
    
    if($respuesta){
        $cantidad_restaurantes=$respuesta['cantidad_restaurantes'];
        // Funciones::imprimeJSON(200,"",$respuesta['cantidad_usuarios']);
    }
    // cantidad de restaurantes

    //cantidad de reservas
    $sql="select count(*) as cantidad_reservas from reserva;";
    $result = $cnx->query($sql);
    $respuesta=$result->fetch(PDO::FETCH_ASSOC);
    
    if($respuesta){
        $cantidad_reservas=$respuesta['cantidad_reservas'];
        // Funciones::imprimeJSON(200,"",$respuesta['cantidad_usuarios']);
    }
    //cantidad de reservas

    $datos_globales=[];

    $datos_globales['c_usuarios']=$cantidad_usuarios;
    $datos_globales['c_restaurantes']=$cantidad_restaurantes;
    $datos_globales['c_reservas']=$cantidad_reservas;


    

    Funciones::imprimeJSON(200,"",$datos_globales);

} catch (Exception $e) {
    Funciones::imprimeJSON(500,$e->getMessage(),"");
}
?>