-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.36-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para proyecto
CREATE DATABASE IF NOT EXISTS `proyecto` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci */;
USE `proyecto`;

-- Volcando estructura para tabla proyecto.menu
CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `imagen` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `precio` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `idrestaurante` int(11) NOT NULL,
  `idcategoria` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idrestaurante` (`idrestaurante`),
  CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`idrestaurante`) REFERENCES `restaurante` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla proyecto.menu: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` (`id`, `nombre`, `descripcion`, `imagen`, `precio`, `idrestaurante`, `idcategoria`) VALUES
	(1, 'Arroz con pato', 'El arroz con pato es una comida típica de la región Lambayeq', 'arrozconpato.jpg', 'S/. 15', 1, 0),
	(2, 'Ensalada especial', 'Plato que se prepara mezclando distintos alimentos, crudos o', 'ensaladaespecial.jpg', 'S/. 5', 1, 0);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;

-- Volcando estructura para tabla proyecto.reserva
CREATE TABLE IF NOT EXISTS `reserva` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `cantidadpersonas` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `idusuario` int(11) NOT NULL,
  `idrestaurante` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idusuario` (`idusuario`),
  KEY `idrestaurante` (`idrestaurante`),
  CONSTRAINT `reserva_ibfk_1` FOREIGN KEY (`idusuario`) REFERENCES `usuarios` (`id`),
  CONSTRAINT `reserva_ibfk_2` FOREIGN KEY (`idrestaurante`) REFERENCES `restaurante` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla proyecto.reserva: ~28 rows (aproximadamente)
/*!40000 ALTER TABLE `reserva` DISABLE KEYS */;
INSERT INTO `reserva` (`id`, `fecha`, `hora`, `cantidadpersonas`, `idusuario`, `idrestaurante`, `estado`) VALUES
	(2, '2018-12-13', '02:30:00', '11', 2, 1, 1),
	(4, '2018-12-15', '02:30:00', '11', 2, 1, 1),
	(5, '2018-12-14', '02:30:00', '11', 2, 1, 1),
	(6, '2018-12-15', '02:30:00', '11', 2, 1, 1),
	(7, '2018-12-14', '02:30:00', '11', 2, 1, 1),
	(8, '2018-12-14', '02:30:00', '11', 2, 1, 1),
	(9, '2018-12-15', '02:30:00', '11', 2, 1, 1),
	(10, '2018-12-13', '02:30:00', '11', 2, 1, 1),
	(11, '2018-12-14', '21:00:00', '3', 2, 1, 1),
	(12, '2018-12-12', '21:00:00', '1', 2, 1, 1),
	(13, '2018-12-15', '21:00:00', '1', 13, 1, 1),
	(14, '2018-12-12', '21:00:00', '1', 13, 1, 1),
	(15, '2018-12-27', '11:11:00', '10', 13, 1, 1),
	(16, '2018-12-06', '07:07:00', '10', 13, 3, 1),
	(17, '2018-04-04', '03:45:00', '10', 13, 3, 1),
	(18, '2018-12-19', '21:00:00', '1', 14, 1, 1),
	(19, '2018-12-14', '21:00:00', '1', 14, 1, 1),
	(20, '2018-12-13', '21:00:00', '1', 14, 2, 1),
	(21, '2018-12-21', '09:00:00', '1', 14, 3, 1),
	(22, '2018-12-13', '14:00:00', '1', 14, 6, 1),
	(23, '2018-12-12', '20:00:00', '1', 14, 1, 1),
	(25, '2018-12-12', '21:00:00', '1', 14, 1, 1),
	(26, '2019-05-18', '14:00:00', '4', 3, 6, 1),
	(33, '2019-05-19', '07:00:00', '3', 15, 4, 1),
	(34, '2019-05-20', '22:00:00', '2', 11, 3, 1),
	(36, '2019-05-30', '02:00:00', '2', 13, 4, 0),
	(37, '2019-05-27', '14:00:00', '2', 14, 2, 1),
	(38, '2019-05-31', '00:00:00', '3', 20, 7, 1);
/*!40000 ALTER TABLE `reserva` ENABLE KEYS */;

-- Volcando estructura para tabla proyecto.restaurante
CREATE TABLE IF NOT EXISTS `restaurante` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `capacidad` varchar(11) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `idtipocomida` int(11) NOT NULL,
  `imagen` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `tipo_usuario` int(11) NOT NULL,
  `usuario` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `clave` char(32) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idtipocomida` (`idtipocomida`),
  CONSTRAINT `restaurante_ibfk_1` FOREIGN KEY (`idtipocomida`) REFERENCES `tipocomida` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla proyecto.restaurante: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `restaurante` DISABLE KEYS */;
INSERT INTO `restaurante` (`id`, `nombre`, `direccion`, `telefono`, `capacidad`, `descripcion`, `idtipocomida`, `imagen`, `tipo_usuario`, `usuario`, `clave`) VALUES
	(1, 'El Rancho', ' Avenida Balta 1115, Chiclayo (Lambayeque)', '074495500', '20', 'Un lugar favorito por su carta variada y buena atención.  Situado en Chiclayo, El Rancho es un resta', 1, 'elrancho.jpg', 1, 'demo', '123'),
	(2, 'El Huaralino', 'Avenida La Libertad 155, Chiclayo (Lambayeque)', '074495500', '5', 'Un lugar que crea y recrea la magia de los sabores peruanos.  Un sitio para vivir con todos los sent', 1, 'elhuaralino.jpg', 1, '', ''),
	(3, 'Romana', 'Avenida José Balta 512, Chiclayo (Lambayeque)', '074495500', '65', 'Deliciosa selección de los mejores platillos de la cocina criolla.  Un sitio para disfrutar con cada', 1, 'romana.jpg', 1, '', ''),
	(4, 'El Rincón del Pato', 'Santa Victoria 250 Jose Carlos Mareategui, Chiclay', '074495500', '20', 'ofrece en cuanto a la carne de pato, platos como pato alverjado, arroz con pato, ceviche de pato, an', 1, 'rincondelpato.jpg', 1, '', ''),
	(5, 'Delicias Marinas', 'Av. Santa Victoria 584,Chiclayo, Chiclayo (Lambaye', '074495500', '120', 'Delicias Marinas cuenta con el más sofisticado servicio de meseros y de cocineros que se encuentran ', 3, 'marina_pescados.jpg', 1, '', ''),
	(6, 'Mi Tía', 'Elías Aguirre 662, Chiclayo (Lambayeque)', '074495500', '2', 'En la carta de Mi Tía encontrarás una gran variedad de ceviches de distintos pescados, cabrito a la ', 5, 'mitia.jpg', 1, '', ''),
	(7, 'asdasd', 'asdasd', '3221312', '12', 'sdfsdf', 1, 'C:fakepath20181218_120437.jpg', 1, '123', '202cb962ac59075b964b07152d234b70');
/*!40000 ALTER TABLE `restaurante` ENABLE KEYS */;

-- Volcando estructura para tabla proyecto.tipocomida
CREATE TABLE IF NOT EXISTS `tipocomida` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comida` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla proyecto.tipocomida: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `tipocomida` DISABLE KEYS */;
INSERT INTO `tipocomida` (`id`, `comida`) VALUES
	(1, 'Peruana'),
	(2, 'Internacional'),
	(3, 'Marina/pescados'),
	(4, 'Piqueos'),
	(5, 'Regional'),
	(6, 'Asiatica'),
	(7, 'Japonesa'),
	(10, 'vegana');
/*!40000 ALTER TABLE `tipocomida` ENABLE KEYS */;

-- Volcando estructura para tabla proyecto.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(60) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla proyecto.user: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `fullname`, `email`, `password`, `image`, `is_active`, `is_admin`, `created_at`) VALUES
	(1, 'demo', 'demo@gmail.com', 'adcd7048512e64b48da55b027577886ee5a36350', 'default.png', 1, 0, '2018-12-12 15:59:54'),
	(9, 'Rebeca NiÃ±o Julca', 'loverebe7@gmail.com', '10470c3b4b1fed12c3baac014be15fac67c6e815', 'default.png', 1, 0, '2019-05-18 03:19:54'),
	(10, 'Grace Santos V', 'g@gmail.com', '123456', NULL, 1, 0, '2019-05-18 00:00:00'),
	(11, 'Maria Suyon', 'maria@gmail.com', '123456', NULL, 1, 0, '0000-00-00 00:00:00'),
	(12, 'jimena ', 'j@gmail.com', '123456', NULL, 1, 0, '0000-00-00 00:00:00'),
	(13, 'lore', 'l@gmail.com', '123456', NULL, 1, 0, '0000-00-00 00:00:00'),
	(14, 'Yessilu Capitan Piscoya', 'y@gmail.com', '123456', NULL, 1, 0, '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Volcando estructura para tabla proyecto.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `clave` char(32) COLLATE utf8_spanish_ci NOT NULL,
  `tipo_usuario` int(11) NOT NULL,
  `foto` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla proyecto.usuarios: ~11 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`id`, `nombre`, `apellido`, `email`, `clave`, `tipo_usuario`, `foto`, `estado`) VALUES
	(2, 'Grace', 'Santos V', 'g@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 3, 'vero.jpg', 1),
	(3, 'Rebeca', 'Niño', 'r@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 3, 'rebeca.jpg', 1),
	(11, 'Maria', 'Suyon', 'maria@gmail.com', '263bce650e68ab4e23f28263760b9fa5', 2, '1210224509logoONU.jpg', 1),
	(13, 'jimena', 'jj', 'j@gmail.com', '202cb962ac59075b964b07152d234b70', 2, 'jimena.jpg', 1),
	(14, 'lore', 'dfklfk', 'l@gmail.com', '202cb962ac59075b964b07152d234b70', 2, 'marinana.jpg', 1),
	(15, 'Yessilu ', 'Capitan Piscoya', 'y@gmail.com', '202cb962ac59075b964b07152d234b70', 2, 'yessilu.PNG', 1),
	(16, 'Maria ', 'Suyon  Farroñan', 'm@gmail.com', '202cb962ac59075b964b07152d234b70', 2, 'maria.jpg', 1),
	(17, 'Isacc', 'Rojas Carlos', 'i@gmail.com', '202cb962ac59075b964b07152d234b70', 2, 'ISAC.jpg', 1),
	(18, 'Jimena', 'Sanchez Rodriguez', 'ji@gmail.com', '202cb962ac59075b964b07152d234b70', 2, 'jimena.jpg', 1),
	(19, 'Katty', 'Espinoza Rojas', 'k@gmail.com', '202cb962ac59075b964b07152d234b70', 2, './uploads/1213143759Katty.jpg', 1),
	(20, 'rebeca', 'niño', 'loverebe7@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 2, './uploads/0511021923avatar.jpg', 1);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
