<?php
header('Access-Control-Allow-Origin: *');
require_once '../conexion/Conexion.clase.php';
require_once '../librerias/lib.php';

try {
    if(
        empty($_POST['p_codigorestaurante']) ||
        empty($_POST['p_codigousuario']) ||
        empty($_POST['p_fechareserva']) ||
        empty($_POST['p_horareserva']) ||
        empty($_POST['p_numpersonasreserva'])
    ){
        Funciones::imprimeJSON(500,"Datos incompletos","");

    }

    $codigo_restaurante=$_POST['p_codigorestaurante'];
    $codigo_usuario=$_POST['p_codigousuario'];
    $fecha_reserva=$_POST['p_fechareserva'];
    $hora_reserva=$_POST['p_horareserva'];
    $num_personas_reserva=$_POST['p_numpersonasreserva'];

    $sql="
    insert into reserva
    (
        fecha,
        hora,
        cantidadpersonas,
        idrestaurante,
        estado,
        idusuario
    )
    value
    (
        '".$fecha_reserva."',
        '".$hora_reserva."',
        '".$num_personas_reserva."',
        '".$codigo_restaurante."',
        1,
        '".$codigo_usuario."'

    );";

    $confirmacion=0;
    $cnx->query($sql) or $confirmacion=1;
    if($confirmacion==0){
        Funciones::imprimeJSON(200,"Reserva agregada","");
    }else{
        Funciones::imprimeJSON(500,"Error al agregar reserva","");
    }
} catch (Exception $e) {
    Funciones::imprimirJSON(500,$e->getMessage(),"");
}

?>