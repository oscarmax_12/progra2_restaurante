<?php
header('Access-Control-Allow-Origin: *');
require_once '../conexion/Conexion.clase.php';
require_once '../librerias/lib.php';


    try {

        

       
        $sql="
        SELECT r.id as codigo_rest ,r.nombre as rest, min(m.precio) as precio,c.cantidad
        from detalle_tipo_comida dtc
            inner join restaurante r on (r.id=dtc.id_restaurante)
            inner join tipocomida tc on (tc.id=dtc.id_tipo_comida)
            inner join menu m on (m.idrestaurante=r.id)
            INNER JOIN calendario c ON r.id=c.id_restaurante
        
        group by 1;";
        $result = $cnx->query($sql);
        $respuesta=$result->fetchAll(PDO::FETCH_ASSOC);
        if($respuesta){
            $ruta="../img_restaurantes/";
            for ($i=0; $i < count($respuesta); $i++) {

                $nombre_archivo=$respuesta[$i]["codigo_rest"];

                if(file_exists($ruta.$nombre_archivo.".jpg")){

                    $respuesta[$i]["foto_restaurante"]=$ruta.$nombre_archivo.".jpg";
                }
                elseif(file_exists($ruta.$nombre_archivo.".jpeg")){
                    $respuesta[$i]["foto_restaurante"]=$ruta.$nombre_archivo.".jpeg";
                }
                elseif(file_exists($ruta.$nombre_archivo.".png")){
                    $respuesta[$i]["foto_restaurante"]=$ruta.$nombre_archivo.".png";
                }
                else{
                    $respuesta[$i]["foto_restaurante"]="no hay archivo";
                }
            }
            Funciones::imprimeJSON(200,"LISTA DE RESTAURANTES INICIAL",$respuesta);
        }else{
            Funciones::imprimeJSON(500,"ERROR AL LISTAR","");
        }
        
    } catch (Exception $e) {
        Funciones::imprimeJSON(500,$e->getMessage(),"");
    }
?>