<?php 
header('Access-Control-Allow-Origin: *');
require_once '../conexion/Conexion.clase.php';
require_once '../librerias/lib.php';

$foto_usuario=$_FILES["foto_usuario"];
$dni=$_POST["txtDni"];
$nombre=$_POST["txtNombre"];
$apellido_paterno=$_POST["txtApellidoPaterno"];
$apellido_materno=$_POST["txtApellidoMaterno"];
$correo=$_POST["txtCorreo"];
$contra=$_POST["txtContra"];
$contra2=$_POST["txtContra2"];

try {
    if (strcmp($contra,$contra2)==0){
        
        $nombre_foto='';

        if($foto_usuario["type"] == "image/jpg"){
            $nombre_foto=$dni.".jpg";
        }
        if($foto_usuario["type"] == "image/jpeg"){
            $nombre_foto=$dni.".jpeg";

        }
        if($foto_usuario["type"] == "image/png"){
            $nombre_foto=$dni.".png";
        }
        if($foto_usuario["type"] == ""){
            echo 'el formato del archivo no es válido';
            exit();
        }
        


        
        $ruta_guardar="../fotos_usuario/".$nombre_foto;
        

            

            $sql="insert into persona

            (   
                nombres,
                apellido_paterno,
                apellido_materno,
                estado,
                foto,
                dni)
            values
            (   
                '".$nombre."',
                '".$apellido_paterno."',
                '".$apellido_materno."',
                'A',
                '".$nombre_foto."',
                '".$dni."');";

            $confirmacion=0;
            $cnx->query($sql) or $confirmacion=1;
            
            if ($confirmacion==0) {
                

                $sql="
                insert into usuarios
                (
                    email,
                    clave,
                    tipo_usuario,
                    foto,
                    estado
                )
                values
                (
                    '".$correo."',
                    '".md5($contra)."',
                    3,
                    '".$nombre_foto."',
                    1
                );
                
                ";

                $confirmacion=0;
                $cnx->query($sql) or $confirmacion=1;
                if($confirmacion==0){
                    

                    $sql_persona=" select MAX(codigo_persona) as id from persona;";
                    $result = $cnx->query($sql_persona);
                    $respuesta_persona=$result->fetch(PDO::FETCH_ASSOC);
                    $id_persona=$respuesta_persona['id'];
                    

                    $sql_usuarios=" select MAX(codigo_usuario) as id from usuarios;";
                    $result = $cnx->query($sql_usuarios);
                    $respuesta_usuarios=$result->fetch(PDO::FETCH_ASSOC);
                    $id_usuarios=$respuesta_usuarios['id'];
                    


                    $sql="insert into detalle_usuario_persona
                    (
                    estado,
                    codigo_persona,
                    codigo_usuario)
                    VALUES
                    (
                    'A',
                    ".$id_persona.",
                    ".$id_usuarios."
                    );";
                    $confirmacion=0;
                    $cnx->query($sql) or $confirmacion=1;
                    if($confirmacion==0){
                        move_uploaded_file($foto_usuario["tmp_name"],$ruta_guardar);
                        echo 'success';

                        // Funciones::imprimeJSON(200,"Exito al agregar usuario","");
                    }else{
                        // Funciones::imprimeJSON(500,"Error al agregar el registro","");
                        echo 'e-save';
                    }

                }else{
                    // Funciones::imprimeJSON(500,"Error al agregar el usuario","");
                    echo 'e-usuario';

                }
            }else{
                // Funciones::imprimeJSON(500,"Error al registrar persona","");
                echo 'e-persona';
            }

    }else{

        // Funciones::imprimeJSON(500,"ERROR CON LAS CONTRASEÑAS","");
        // echo 'las contraseñas no coinciden';
        echo 'e-contrasena';
        // exit();
    }
} catch (Exception $e) {

    Funciones::imprimeJSON(500,$e->getMessage(),"");
}

?>