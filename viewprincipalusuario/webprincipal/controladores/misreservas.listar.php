<?php

header('Access-Control-Allow-Origin: *');
require_once '../conexion/Conexion.clase.php';
require_once '../librerias/lib.php';


try {

    if (
            empty($_POST['p_user'])
    ) {
        Funciones::imprimeJSON(500, "Faltan completar datos", "");
        exit();
    }
    $cod = $_POST['p_user'];

    $sql = "
                SELECT r1.id AS codigo_reservas,r1.fecha,r1.cantidadpersonas,r2.nombre,r2.id AS codigo_rest
		  FROM reserva r1
		  INNER JOIN restaurante r2 ON r1.idrestaurante=r2.id
		  where r1.idusuario=".$cod."
                  ";
    $result = $cnx->query($sql);
    $respuesta = $result->fetchAll(PDO::FETCH_ASSOC);
    if ($respuesta) {
        $ruta = "../img_restaurantes/";
        for ($i = 0; $i < count($respuesta); $i++) {

            $nombre_archivo = $respuesta[$i]["codigo_rest"];

            if (file_exists($ruta . $nombre_archivo . ".jpg")) {

                $respuesta[$i]["foto_restaurante"] = $ruta . $nombre_archivo . ".jpg";
            } elseif (file_exists($ruta . $nombre_archivo . ".jpeg")) {
                $respuesta[$i]["foto_restaurante"] = $ruta . $nombre_archivo . ".jpeg";
            } elseif (file_exists($ruta . $nombre_archivo . ".png")) {
                $respuesta[$i]["foto_restaurante"] = $ruta . $nombre_archivo . ".png";
            } else {
                $respuesta[$i]["foto_restaurante"] = "no hay archivo";
            }
        }
        Funciones::imprimeJSON(200, "LISTA DE RESTAURANTES INICIAL", $respuesta);
    } else {
        Funciones::imprimeJSON(500, "ERROR AL LISTAR", "");
    }
} catch (Exception $e) {
    Funciones::imprimeJSON(500, $e->getMessage(), "");
}
?>