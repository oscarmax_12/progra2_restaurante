$(document).ready (function(){
    cargarComboTipoComida('#cboTipoComida','seleccione');
  })
  
  $('#btnBuscarRest').on('click',function (){
    const ruta="../controladores/restaurante.buscar.php";
    let tipoComida =$('#cboTipoComida').val();
    let fecha =$('#txtfecha').val();

    if(typeof fecha== "undefined"|| fecha==""){
      
      fecha=ObtenerFecha()+' 10:00:00';
    }else{
      fecha=$('#txtfecha').val()+' 10:00:00';
    }
    
    
    $.post(
      ruta,
      {
          p_tipo_comida: tipoComida,
          p_fecha:fecha
      }
      

    ).done(function(resultado){

      

        if(resultado.estado==200){
            
          
          localStorage.setItem('lista_restaurantes',JSON.stringify(resultado.datos));
          location.href='restaurante.php';

        }
        
        // else{
        //     alert(resultado.mensaje);
        // }
        
    }).fail(function(error){

      var datosJSON = $.parseJSON(error.responseText);
        alert( datosJSON.mensaje);
    
           
    })
  })

  function ObtenerFecha(){

      var fecha = new Date(); //Fecha actual
      var mes = fecha.getMonth()+1; //obteniendo mes
      var dia = fecha.getDate(); //obteniendo dia
      var anio = fecha.getFullYear(); //obteniendo año
      if(dia<10){
        dia='0'+dia;
       } //agrega cero si el menor de 10
      if(mes<10){
        mes='0'+mes //agrega cero si el menor de 10
      }
    
    return anio+"-"+mes+"-"+dia;
  }