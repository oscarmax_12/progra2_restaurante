$(document).ready(function(){
    
    if(Cookies.get('id')){
        $('#txtNombreUsuario').html(Cookies.get('nombre_usuario'));
        $('#imgUsuario').attr("src",Cookies.get('ruta_foto'));
        console.log(Cookies.get('ruta_foto'));
    }
    else{
        $('#txtNombreUsuario').html('BIENVEINDO');
    }
})

$('#btnCerrarSesion').on('click',function(){
    Cookies.remove('id');
    Cookies.remove('nombre_usuario');
    
    Cookies.remove('ruta_foto');
    location.reload();
})

$('#linkRest').on('click',function(){
    if(localStorage.getItem('lista_restaurantes')){
        localStorage.removeItem('lista_restaurantes');
    }
    location.href='restaurante.php';
})