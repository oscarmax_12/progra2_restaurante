$(document).ready(function(){
    if(localStorage.getItem('lista_restaurantes')){
        filtrado();
    }else{
        listado();
    }
    
})
function filtrado(){
    let lista_restaurantes = JSON.parse(localStorage.getItem('lista_restaurantes'));
    console.log(lista_restaurantes);
    let html='';

    $.each(lista_restaurantes,function(i,item){
        html+='<li class="item">';
        html+='<div class="lst_img">';
        html+='<img src="'+item.foto_restaurante+'" alt="imagenrest">';
        html+='</div>'

        html+='<div class="lst_descripcion">';
        html+='<p class="lst_descripcion-title">'+item.rest+'</p>';
        html+='<p class="lst_descripcion-precio">Desde: S/.'+item.precio+'</p>';
        html+='<a href="#" class="btn-rest" onclick="inicio_reserva('+item.codigo_rest+')" >Hacer reserva</a>';
        html+='</div>'
        html+='</li>';

    })
    $('#lst_restaurantes').html(html);


    // FALTA EL DISEÑO DEL LISTADO
    

}
function listado(){
    const ruta="../controladores/restaurante.listar.php";
    $.post(
        ruta      
  
    ).done(function(resultado){
  
        
  
          if(resultado.estado==200){
            console.log(resultado)

            let  datosJSON = resultado.datos;
            let html='';

            $.each(datosJSON,function(i,item){
                html+='<li class="item">';
                html+='<div class="lst_img">';
                html+='<img src="'+item.foto_restaurante+'" alt="imagenrest">';
                html+='</div>'

                html+='<div class="lst_descripcion">';
                html+='<p class="lst_descripcion-title">'+item.rest+'</p>';
                html+='<p class="lst_descripcion-precio">Desde: S/.'+item.precio+'</p>';
                html+='<a href="#" class="btn-rest" onclick="inicio_reserva('+item.codigo_rest+')" >Hacer reserva</a>';
                html+='</div>'
                html+='</li>';

            })
            $('#lst_restaurantes').html(html);
          }else{
              console.log(resultado);
              console.log('error');
          }
          
      }).fail(function(error){
              console.log(error.responseJSON);
      })
}

function inicio_reserva(codigorest){
    console.log(codigorest);
    window.localStorage.setItem('codigo_rest',codigorest);
    location.href='reserva.php';
}   