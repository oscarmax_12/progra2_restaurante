$(document).ready(function(){
     if(Cookies.get('id')){
       location.href='index.php';
       
     }
   })
$('#frmInicioSesion').submit(function(e){
    e.preventDefault();
    let correo=$('#txtcorreo').val();
    let contrasena=$('#txtcontra').val();
    const ruta="../controladores/sesion.php";

    $.post(
        ruta,
        {
             p_correo:correo,
             p_contrasena: contrasena

        }

   ).done(function(resultado){

        if(resultado.estado==200){
             var datosJSON=resultado.datos;
             
          
          Cookies.set('id',datosJSON.codigo_usuario);
          Cookies.set('nombre_usuario',datosJSON.nombre_usuario);
          
          Cookies.set('ruta_foto',datosJSON.ruta_foto);
          
          
             location.href='index.php';
          
        }else{
             alert('FALTAN COMPLETAR DATOS PARA REALIZAR LA OPERACIÓN');
        }
        
        

        
   }).fail(function(error){
        console.log(error.responseJSON);
   })

})