$(document).ready(function(){
  
        listado();
    
    
})

function listado(){
    const ruta="../controladores/misreservas.listar.php";
    let codigo_usuario= Cookies.get('id');
    $.post(
        ruta,
        {
            p_user:codigo_usuario
        }      
  
    ).done(function(resultado){
  
        
  
          if(resultado.estado==200){
            console.log(resultado)

            let  datosJSON = resultado.datos;
            let html='';

            $.each(datosJSON,function(i,item){
                html+='<li class="item">';
                html+='<div class="lst_img">';
                html+='<img src="'+item.foto_restaurante+'" alt="imagenrest">';
                html+='</div>'

                html+='<div class="lst_descripcion">';
                html+='<p class="lst_descripcion-title">'+item.nombre+'</p>';
                html+='<p class="lst_descripcion-precio">Fecha de Reserva: '+item.fecha+'</p>';
                html+='<p class="lst_descripcion-precio">Cant. Personas: '+item.cantidadpersonas+'</p>';
                // html+='<a href="#" class="btn-rest" onclick="inicio_reserva('+item.codigo_rest+')" >Hacer reserva</a>';
                html+='</div>'
                html+='</li>';

            })
            $('#lst_restaurantes').html(html);
          }else{
              console.log(resultado);
              console.log('error');
          }
          
      }).fail(function(error){
              console.log(error.responseJSON);
      })
}

function inicio_reserva(codigorest){
    console.log(codigorest);
    window.localStorage.setItem('codigo_rest',codigorest);
    location.href='reserva.php';
}   