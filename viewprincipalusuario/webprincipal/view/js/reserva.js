$('#btnReserva').on('click',function(){
    if(Cookies.get('id')){
        doReserva();
    }
    else{
        alert('DEBES INICIAR SESION ');
    }
})

function doReserva(){
    ruta="../controladores/reserva.agregar.php";
    let codigo_restaurante=window.localStorage.getItem('codigo_rest');
    let codigo_usuario= Cookies.get('id');
    let fecha_reserva=$('#txtFechaReserva').val();
    let hora_reserva=$('#txtHoraReserva').val();
    let num_personas_reserva=$('#txtNumPersonasReserva').val();
    console.log(codigo_restaurante,codigo_usuario,fecha_reserva,hora_reserva,num_personas_reserva);

    $.post(
        ruta,
        {
            p_codigorestaurante: codigo_restaurante,
            p_codigousuario: codigo_usuario,
            p_fechareserva: fecha_reserva,
            p_horareserva: hora_reserva,
            p_numpersonasreserva: num_personas_reserva

        }

   ).done(function(resultado){

        if(resultado.estado==200){
            var datosJSON = resultado;
            console.log(datosJSON);
            $('#txtFechaReserva').val('');
            $('#txtHoraReserva').val('');
            $('#txtNumPersonasReserva').val('');
            alert('RESERVA REGISTRADA');
            
          
        }else{
            console(resultado);
            alert(resultado);
            
        }
        
        

        
   }).fail(function(error){
        console.log(error);
   })

}