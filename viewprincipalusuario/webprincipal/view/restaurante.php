<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="description" content="CareMed demo project">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Food Page | Restaurantes</title>
<link href="tools/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="tools/jquery.min.js"></script>
<!-- Custom Theme files -->
<link href="tools/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- Custom Theme files -->
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!--webfont-->
<link href='//fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Lobster+Two:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<!--Animation-->
<script src="tools/wow.min.js"></script>
<link href="tools/animate.css" rel='stylesheet' type='text/css' />
<script>
	new WOW().init();
</script>

<script type="text/javascript" src="tools/move-top.js"></script>
<script type="text/javascript" src="tools/easing.js"></script>
<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
				});
			});
		</script>

<link href="css/stylesrestaurante.css" rel='stylesheet' type='text/css' />
<script  src = " https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js " > </script>
<link href="css/stylescabecera.css" rel='stylesheet' type='text/css'>


</head>
<body>
    <!-- header-section-starts -->
	<div class="header">
		
	<?php
			if(!isset($_COOKIE['id'])){
				include_once 'cabecera.out.php';
			}else{
				include_once 'cabecera.php';
			}
		?>


		
	</div>
	<!-- header-section-ends -->

	<div class="content">
		<div class="ordering-section-head text-center wow bounceInRight" data-wow-delay="0.4s">
			<h3 class="title">Resultado de la búsqueda</h3>

			<ul id="lst_restaurantes" class="lista_res">
				<li class="item">
					<div class="lst_img">
						<img src="../fotos_usuario/71195649.jpeg" alt="imagenrest">
					</div>
					
					<div class="lst_descripcion">
						<p class="lst_descripcion-title">Nombre restaurante</p>
						<p class="lst_descripcion-precio">Precio</p>
						<a href="" class="btn-rest">Link del restaurante</a>
					</div>

				</li>
			</ul>
				
		</div>
	</div>


	

	<!-- content-section-starts -->
	<div class="content">
		
		<div class="contact-section" id="contact">
			<div class="container">
				<div class="contact-section-grids">
					<div class="col-md-3 contact-section-grid wow fadeInLeft" data-wow-delay="0.4s">
						<h4>Site Links</h4>
						<ul>
							<li><i class="point"></i></li>
							<li class="data"><a href="#">About Us</a></li>
						</ul>
						<ul>
							<li><i class="point"></i></li>
							<li class="data"><a href="#">Contact Us</a></li>
						</ul>
						<ul>
							<li><i class="point"></i></li>
							<li class="data"><a href="#">Privacy Policy</a></li>
						</ul>
						<ul>
							<li><i class="point"></i></li>
							<li class="data"><a href="#">Terms of Use</a></li>
						</ul>
					</div>
					<div class="col-md-3 contact-section-grid wow fadeInLeft" data-wow-delay="0.4s">
						<h4>Site Links</h4>
						<ul>
							<li><i class="point"></i></li>
							<li class="data"><a href="#">About Us</a></li>
						</ul>
						<ul>
							<li><i class="point"></i></li>
							<li class="data"><a href="#">Contact Us</a></li>
						</ul>
						<ul>
							<li><i class="point"></i></li>
							<li class="data"><a href="#">Privacy Policy</a></li>
						</ul>
						<ul>
							<li><i class="point"></i></li>
							<li class="data"><a href="#">Terms of Use</a></li>
						</ul>
					</div>
					<div class="col-md-3 contact-section-grid wow fadeInRight" data-wow-delay="0.4s">
						<h4>Follow Us On...</h4>
						<ul>
							<li><i class="fb"></i></li>
							<li class="data"> <a href="#">  Facebook</a></li>
						</ul>
						<ul>
							<li><i class="tw"></i></li>
							<li class="data"> <a href="#">Twitter</a></li>
						</ul>
						<ul>
							<li><i class="in"></i></li>
							<li class="data"><a href="#"> Linkedin</a></li>
						</ul>
						<ul>
							<li><i class="gp"></i></li>
							<li class="data"><a href="#">Google Plus</a></li>
						</ul>
					</div>
					<div class="col-md-3 contact-section-grid nth-grid wow fadeInRight" data-wow-delay="0.4s">
						<h4>Subscribe Newsletter</h4>
						<p>To get latest updates and food deals every week</p>
						<input type="text" class="text" value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = '';}">
						<input type="submit" value="submit">
						</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- content-section-ends -->
	<!-- footer-section-starts -->
	<?php include_once 'footer.php'?>
	<!-- footer-section-ends -->
	  <script type="text/javascript">
						$(document).ready(function() {
							/*
							var defaults = {
					  			containerID: 'toTop', // fading element id
								containerHoverID: 'toTopHover', // fading element hover id
								scrollSpeed: 1200,
								easingType: 'linear' 
					 		};
							*/
							
							$().UItoTop({ easingType: 'easeOutQuart' });
							
						});
					</script>
				<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

				<script src="js/cabecera.js"></script>

				<script src="js/restaurante.js"></script>


</body>
</html>