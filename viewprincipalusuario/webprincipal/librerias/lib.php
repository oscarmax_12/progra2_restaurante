<?php
class Funciones{
    public static function imprimeJSON($estado, $mensaje,$datos){
        header("HTTP/1.1 " . $estado);
        header('Content-Type: application/json');

        $response["estado"] = $estado;
        $response["mensaje"] = $mensaje;
        $response["datos"] = $datos;
        echo json_encode($response);
    }
}
