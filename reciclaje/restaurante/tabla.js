$(document).ready(function () {
    listar();
});

function listar() {
    $.post("lista2.php").done(function (resultado) {
        var datosJSON = resultado;
        
           
                html = "";
            html += '<table id="tbl" class="table table-striped table-bordered table-hover dt-responsive" width="100%">';
            html += '<thead>';
            html += '<tr style="background-color: #ededed; height: 40px;">';
            html += '<th style="text-align: center">id </th>';
            html += '<th style="text-align: center">Nombre </th>';
            html += '<th style="text-align: center">Direccion</th>';
           html += '<th style="text-align: center">Telefono</th>';
            html += '<th style="text-align: center">Capacidad</th>';
           html += '<th style="text-align: center">Descripcion</th>';
           html += '<th style="text-align: center">Tipo Comida </th>';
            html += '<th style="text-align: center">Imagen</th>';
           html += '<th style="text-align: center">Tipo usuario</th>';
            html += '<th style="text-align: center">Usuario</th>';
           html += '<th style="text-align: center">Clave</th>';
            html += '<th style="text-align: center">Acciones</th>';

            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';
            $.each(datosJSON.datos, function (i, item) {

                html += '<tr>';
                html += '<td align="center">' + item.id + '</td>';
                html += '<td align="center">' + item.nombre + '</td>';
               html += '<td align="center">' + item.direccion + '</td>';
               html += '<td align="center">' + item.telefono + '</td>';
               html += '<td align="center">' + item.capacidad + '</td>';
               html += '<td align="center">' + item.descripcion + '</td>';
                  html += '<td align="center">' + item.tipocomida + '</td>';
               html += '<td align="center">' + item.imagen + '</td>';
               html += '<td align="center">' + item.tipo_usuario + '</td>';
               html += '<td align="center">' + item.usuario + '</td>';
               html += '<td align="center">' + item.clave + '</td>';
                html += '<td>                <button class="btn btn-danger" onclick="eliminar('+item.id+')">Eliminar</button>                <button class="btn btn-primary" onclick="editar('+item.id+')">Editar</button>            </td>';
                
                

                html += '</tr>';
            });
            html += '</tbody>';
            html += '</table>';
            
            $("#tabla").html(html);
            $("#tbl").DataTable();
        
    }).fail(function (error) {
        var datosJSON = JSON.stringify(error);
        alert("error")
        
    });
}

function eliminar(id){
    $.ajax({
        type: "POST",
        url: "eliminar.php",
        data: { "id":id },
        success: function (response) {
            console.log(response);
            if(response==0){
                listar();
            } else {
                alert("Error al eliminar el registro!");
            }
        }
    });
}

function insertar(){
	var nom 	= $("#nombre").val();
	var dir 	= $("#direccion").val();
	var tel 	= $("#telefono").val();
	var cap 	= $("#capacidad").val();
	var des 	= $("#descripcion").val();
	var tip 	= $("#cbotipocomida").val();

	var tip 	= $("#tipo_usuario").val();
	var usu 	= $("#usuario").val();
	var cla 	= $("#clave").val();
	

	$.ajax({
		type: "POST",
		url: "guardar.php",
		data: {"nombre":nom,"direccion":dir,"telefono":tel,"capacidad":cap,"descripcion":des,
		"idtipocomida":tip,"imagen":ima,"tipo_usuario":tip,"usuario":usu,"clave":cla},
		success: function (response) {
			console.log(response);
			if(response==0){
				listar();
				$("#formulario").modal('toggle');
				limpiar();
			} else {
				alert("Error al registrar los datos!");
			}
		}
	});

}

function editar(id){
    $.ajax({
        type: "POST",
        url: "editar.php",
        data: { "id":id },
        success: function (response) {
            console.log(response);
            var data = JSON.parse(response);
            $("#id").val(id);
            $("#nombre").val(data.nombre);
             $("#direccion").val(data.direccion);
              $("#telefono").val(data.telefono);
               $("#capacidad").val(data.capacidad);
              $("#descripcion").val(data.descripcion);
               $("#cbotipocomida").val(data.idtipocomida);
                $("#tipo_usuario").val(data.tipo_usuario);
                 $("#usuario").val(data.usuario);
                  $("#clave").val(data.clave);
            
            $("#formulario").modal('toggle');
        }
    });
}

function guardar(){
	var id = $("#id").val();
	if(id>0){
		actualizar();
	} else {
		insertar();
	}
}

function actualizar(){
	var id 		= $("#id").val();
	var nom 	= $("#nombre").val();
	var dir		= $("#direccion").val();
	var tel 	= $("#telefono").val();
	var cap 	= $("#capacidad").val();
	var des  	= $("#descripcion").val();
	var idtip 	= $("#cbotipocomida").val();
	var ima 	= $("#imagen").val();
	var tip 	= $("#tipo_usuario").val();
	var usu 	= $("#usuario").val();
	var cla		= $("#clave").val();
	
	$.ajax({
		type: "POST",
		url: "actualizar.php",
		data: {"id":id, "nombre":nom,"direccion":dir, "telefono":tel,"capacidad":cap,
		 "descripcion":des,"idtipocomida":idtip,"imagen":ima,
		"tipo_usuario":tip, "usuario":usu,"clave":cla},
		success: function (response) {
			console.log(response);
			if(response==0){
				listar();
				$("#formulario").modal('toggle');
				limpiar();
			} else {
				alert("Error al editar los datos!");
			}
		}
	});
}


function limpiar(){
	$("#id").val('');
	$("#nombre").val('');
	$("#direccion").val('');
	$("#telefono").val('');
	$("#capacidad").val('');
	$("#descripcion").val('');
	$("#cbotipocomida").val('');
	$("#imagen").val('');
	$("#tipo_usuario").val('');
	$("#usuario").val('');
	$("#clave").val('');
	
}