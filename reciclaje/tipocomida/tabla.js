$(document).ready(function () {
    listar();
});

function listar() {
    $.post("lista2.php").done(function (resultado) {
        var datosJSON = resultado;
        
           
                html = "";
            html += '<table id="tbl" class="table table-striped table-bordered table-hover dt-responsive" width="100%">';
            html += '<thead>';
            html += '<tr style="background-color: #ededed; height: 40px;">';
            html += '<th style="text-align: center">id </th>';
            html += '<th style="text-align: center">Nombre </th>';
           // html += '<th style="text-align: center">Apellidos</th>';
           // html += '<th style="text-align: center">Email</th>';
           // html += '<th style="text-align: center">Telefono</th>';
           // html += '<th style="text-align: center">Direccion</th>';
            html += '<th style="text-align: center">Acciones</th>';

            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';
            $.each(datosJSON.datos, function (i, item) {

                html += '<tr>';
                html += '<td align="center">' + item.id + '</td>';
                html += '<td align="center">' + item.comida + '</td>';
               // html += '<td align="center">' + item.apellidos + '</td>';
               // html += '<td align="center">' + item.email + '</td>';
                //html += '<td align="center">' + item.telefono + '</td>';
               // html += '<td align="center">' + item.direccion + '</td>';
                html += '<td>                <button class="btn btn-danger" onclick="eliminar('+item.id+')">Eliminar</button>                <button class="btn btn-primary" onclick="editar('+item.id+')">Editar</button>            </td>';
                
                

                html += '</tr>';
            });
            html += '</tbody>';
            html += '</table>';
            
            $("#tabla").html(html);
            $("#tbl").DataTable();
        
    }).fail(function (error) {
        var datosJSON = JSON.stringify(error);
        alert("error")
        
    });
}

function eliminar(id){
    $.ajax({
        type: "POST",
        url: "eliminar.php",
        data: { "id":id },
        success: function (response) {
            console.log(response);
            if(response==0){
                listar();
            } else {
                alert("Error al eliminar el registro!");
            }
        }
    });
}

function insertar(){
	var com 	= $("#comida").val();
	

	$.ajax({
		type: "POST",
		url: "guardar.php",
		data: {"comida":com},
		success: function (response) {
			console.log(response);
			if(response==0){
				listar();
				$("#formulario").modal('toggle');
				limpiar();
			} else {
				alert("Error al registrar los datos!");
			}
		}
	});

}

function editar(id){
    $.ajax({
        type: "POST",
        url: "editar.php",
        data: { "id":id },
        success: function (response) {
            console.log(response);
            var data = JSON.parse(response);
            $("#id").val(id);
            $("#comida").val(data.comida);
            
            $("#formulario").modal('toggle');
        }
    });
}

function guardar(){
	var id = $("#id").val();
	if(id>0){
		actualizar();
	} else {
		insertar();
	}
}

function actualizar(){
	var id 		= $("#id").val();
	var com 	= $("#comida").val();
	

	$.ajax({
		type: "POST",
		url: "actualizar.php",
		data: {"id":id, "comida":com},
		success: function (response) {
			console.log(response);
			if(response==0){
				listar();
				$("#formulario").modal('toggle');
				limpiar();
			} else {
				alert("Error al editar los datos!");
			}
		}
	});
}


function limpiar(){
	$("#id").val('');
	$("#comida").val('');
	
}