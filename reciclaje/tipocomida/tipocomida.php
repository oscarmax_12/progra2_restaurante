<?php
session_start();
if(!isset($_SESSION['user_id'])) header("location: home.php")
?>

<!DOCTYPE html>
<html>
<head>
	<title>TIPO DE COMIDA</title>


	
	<!-- PARA DATATABLES -->

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/dataTables.bootstrap4.css" /> -->
	<!-- <link rel="stylesheet" href="bootstrap.min.css" /> -->
	<link rel="stylesheet" href="dataTables.bootstrap4.css" />
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/dataTables.bootstrap4.css" /> -->
<!-- 	<script type="text/javascript" src="tabla.js"></script>
	<script type="text/javascript">
		window.onload = listar();
	</script> -->
</head>
<body>
<div class="container">
	<blockquote>
    <a href="../home.php"><span class="stylesheet"><= Regresar a Página Principal</span></a>
</blockquote>
	<h1>TIPO DE COMIDA</h1>

	<nav class="navbar navbar-default">
		<button class="btn btn-success" data-toggle="modal" data-target="#formulario">Nuevo</button>
	</nav>

	<div id="formulario" class="modal fade" role="dialog">

	<div class="modal-dialog">

		<div class="modal-content">
		<div class="modal-head">
			<h4> TIPO DE COMIDA</h4>
		</div>

		<div class="modal-body">
			
			<input type="hidden" id="id" name="id" value="">

			<div class="form-group">
			<label for="comida">Nombres</label>
			<input class="form-control" type="text" name="comida" id="comida">
			</div>
			
	
			<button id="btnguardar" onclick="guardar()" class="btn btn-primary">Guardar</button>
			</div>
		</div>
	</div>
	</div>


	<div id="tabla" class="table table-striped table-bordered" style="width:80%">
	</div>
</div>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script> -->
<script src="jquery-3.3.1.js"></script>

<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->


<script src="popper.min.js" type="text/javascript"></script>
<script src="bootstrap.min.js" type="text/javascript"></script>

<!-- <script src="jquery.dataTables.min.js" type="text/javascript"></script> -->
<script src="b4.jquery.dataTables.min.js" type="text/javascript"></script>




<!-- <script src="dataTables.bootstrap.min.js" type="text/javascript"></script> -->
<script src="dataTables.bootstrap4.min.js" type="text/javascript"></script>

<script src="tabla.js" type="text/javascript"></script>
</body>
</html>