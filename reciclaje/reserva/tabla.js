$(document).ready(function () {
    listar();
});

function listar() {
    $.post("lista2.php").done(function (resultado) {
        var datosJSON = resultado;
        
           
                html = "";
            html += '<table id="tbl" class="table table-striped table-bordered table-hover dt-responsive" width="100%">';
            html += '<thead>';
            html += '<tr style="background-color: #ededed; height: 40px;">';
            html += '<th style="text-align: center">Id </th>';
            html += '<th style="text-align: center">Fecha </th>';
            html += '<th style="text-align: center">Hora</th>';
           	html += '<th style="text-align: center">Cantidad Personas</th>';
           	html += '<th style="text-align: center">Usuario</th>';
            html += '<th style="text-align: center">Restaurante</th>';
            html += '<th style="text-align: center">Estado</th>';
            html += '<th style="text-align: center">Acciones</th>';

            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';
            $.each(datosJSON.datos, function (i, item) {

               html += '<tr>';
               html += '<td align="center">' + item.id + '</td>';
               html += '<td align="center">' + item.fecha + '</td>';
               html += '<td align="center">' + item.hora + '</td>';
               html += '<td align="center">' + item.cantidadpersonas + '</td>';
               html += '<td align="center">' + item.usuarios + '</td>';
               html += '<td align="center">' + item.restaurante + '</td>';
               html += '<td align="center">' + item.estado + '</td>';
               html += '<td>                <button class="btn btn-danger" onclick="eliminar('+item.id+')">Eliminar</button>                <button class="btn btn-primary" onclick="editar('+item.id+')">Editar</button>            </td>';
                
                

                html += '</tr>';
            });
            html += '</tbody>';
            html += '</table>';
            
            $("#tabla").html(html);
            $("#tbl").DataTable();
        
    }).fail(function (error) {
        var datosJSON = JSON.stringify(error);
        alert("error")
        
    });
}

function eliminar(id){
    $.ajax({
        type: "POST",
        url: "eliminar.php",
        data: { "id":id },
        success: function (response) {
            console.log(response);
            if(response==0){
                listar();
            } else {
                alert("Error al eliminar el registro!");
            }
        }
    });
}

function insertar(){
	var fec 	= $("#fecha").val();
	var hor 	= $("#hora").val();
	var can 	= $("#cantidadpersonas").val();
	var cbousu 	= $("#cbousuarios").val();
	var cbores 	= $("#cborestaurante").val();
	var est 	= $("#estado").val();

	

	$.ajax({
		type: "POST",
		url: "guardar.php",
		data: {"fecha":fec,"hora":hor,"cantidadpersonas":can,"idusuario":cbousu,"idrestaurante":cbores,
		"estado":est},
		success: function (response) {
			console.log(response);
			if(response==0){
				listar();
				$("#formulario").modal('toggle');
				limpiar();
			} else {
				alert("Error al registrar los datos!");
			}
		}
	});

}

function editar(id){
    $.ajax({
        type: "POST",
        url: "editar.php",
        data: { "id":id },
        success: function (response) {
            console.log(response);
            var data = JSON.parse(response);
            $("#id").val(id);
            $("#fecha").val(data.fecha);
            $("#hora").val(data.hora);
           	$("#cantidadpersonas").val(data.cantidadpersonas);
            $("#cbousuarios").val(data.cbousuarios);
            $("#cborestaurante").val(data.cborestaurante);
             $("#estado").val(data.estado);
            
            
            $("#formulario").modal('toggle');
        }
    });
}

function guardar(){
	var id = $("#id").val();
	if(id>0){
		actualizar();
	} else {
		insertar();
	}
}

function actualizar(){
	var id 		= $("#id").val();
	var fec 	= $("#fecha").val();
	var hor 	= $("#hora").val();
	var can 	= $("#cantidadpersonas").val();
	var cbousu	= $("#cbousuarios").val();
	var cbores 	= $("#cborestaurante").val();
	var est 	= $("#estado").val();

	

	$.ajax({
		type: "POST",
		url: "actualizar.php",
		data: {"id":id, "fecha":fec,"hora":hor, "cantidadpersonas":can,"idusuario":cbousu,
		 "idrestaurante":cbores,"estado":est},
		success: function (response) {
			console.log(response);
			if(response==0){
				listar();
				$("#formulario").modal('toggle');
				limpiar();
			} else {
				alert("Error al editar los datos!");
			}
		}
	});
}


function limpiar(){
	$("#id").val('');
	$("#fecha").val('');
	$("#hora").val('');
	$("#cantidadpersonas").val('');
	$("#cbousuarios").val('');
	$("#cborestaurante").val('');
	$("#estado").val('');
	

	
}