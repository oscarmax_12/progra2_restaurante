<?php
session_start();
if(!isset($_SESSION['user_id'])) header("location: ../index.php")
?>

<!DOCTYPE html>
<html>
<head>
	<title>RESTAURANTE</title>


	
	<!-- PARA DATATABLES -->
	<script src="jquery-3.3.1.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/dataTables.bootstrap4.css" /> -->
	<!-- <link rel="stylesheet" href="bootstrap.min.css" /> -->
	<link rel="stylesheet" href="dataTables.bootstrap4.css" />
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/dataTables.bootstrap4.css" /> -->
	<script type="text/javascript" src="tabla.js"></script>
	<!-- <script type="text/javascript">
		window.onload = listar();
	</script> -->


	<script>
$(document).ready(function() {
    $(".upload").on('click', function() {
        var formData = new FormData();
        var files = $('#image')[0].files[0];
        formData.append('file',files);
        formData.append('nombre', $("#nombre").val());
        $.ajax({
            url: 'upload.php',
            type: 'post',
            data: formData,
            contentType: false,
            processData: false,
            success: function(response) 
            {
            	
            }
        });
		return false;
    });
});
</script>

</head>
<body>
<div class="container">
	<blockquote>
    <a href="../home.php"><span class="stylesheet"><= Regresar a Página Principal</span></a>
	</blockquote>
	<h1>RESTAURANTE</h1>

	<nav class="navbar navbar-default">
		<button class="btn btn-success" data-toggle="modal" data-target="#formulario">Nuevo</button>
	</nav>

	<div id="formulario" class="modal fade" role="dialog">

	<div class="modal-dialog">

		<div class="modal-content">
		<div class="modal-head">
			<h4>RESTAURANTE</h4>
		</div>

		<div class="modal-body">
			
			<input type="hidden" id="id" name="id" value="">

			<div class="form-group">
			<label for="nombre">Nombres</label>
			<input class="form-control" type="text" name="nombre" id="nombre">
			</div>

			<div class="form-group">
			<label for="direccion">Direccion</label>
			<input class="form-control" type="text" name="direccion" id="direccion">
			</div>

			<div class="form-group">
			<label for="telefono">Telefono</label>
			<input class="form-control" type="text" name="telefono" id="telefono">
			</div>

			<div class="form-group">
			<label for="capacidad">Capacidad</label>
			<input class="form-control" type="text" name="capacidad" id="capacidad">
			</div>

				<div class="form-group">
			<label for="descripcion">Descripcion</label>
			<input class="form-control" type="text" name="descripcion" id="descripcion">
			</div>


			 <div class="form-group">
			        <label>Selecciona Tipo Comida</label>
			        <?php require "listatipocomida.php" ?>
			</div>

			<div class="form-group">
							<label for="image">Nueva imagen</label>
							<input type="file" class="form-control-file" name="image" id="image">
					</div>

	<div class="form-group">
			<label for="tipo_usuario">Tipo Usuario</label>
			<input class="form-control" type="text" name="tipo_usuario" id="tipo_usuario">
			</div>

	<div class="form-group">
			<label for="usuario">Usuario</label>
			<input class="form-control" type="text" name="usuario" id="usuario">
			</div>

	<div class="form-group">
			<label for="clave">Clave</label>
			<input class="form-control" type="text" name="clave" id="clave">
			</div>

			
	
			<button id="btnguardar" onclick="guardar()" class="btn btn-primary">Guardar</button>
			</div>
		</div>
	</div>
	</div>


	<div id="tabla" class="table table-striped table-bordered" style="width:120%">
	</div>
</div>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script> -->


<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->


<script src="popper.min.js" type="text/javascript"></script>
<script src="bootstrap.min.js" type="text/javascript"></script>

<!-- <script src="jquery.dataTables.min.js" type="text/javascript"></script> -->
<script src="b4.jquery.dataTables.min.js" type="text/javascript"></script>




<!-- <script src="dataTables.bootstrap.min.js" type="text/javascript"></script> -->
<script src="dataTables.bootstrap4.min.js" type="text/javascript"></script>

<script src="tabla.js" type="text/javascript"></script>
</body>
</html>