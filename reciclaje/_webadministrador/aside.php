    <aside class="main-sidebar"><!-- Left side column. contains the logo and sidebar -->
        <section class="sidebar"><!-- sidebar: style can be found in sidebar.less -->
            <div class="user-panel"> <!-- Sidebar user panel -->
                <div class="pull-left image">
                    <img src="images/profiles/<?php echo $profile_pic; ?>" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><?php echo $fullname; ?></p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <ul class="sidebar-menu"><!-- sidebar menu: : style can be found in sidebar.less -->
                <li class="header">NAVEGACIÓN</li>

                <li class="<?php if(isset($active1)){echo $active1;}?>">
                    <a href="home.php"><i class="fa fa-user"></i> <span>Mi perfil</span></a>
                </li>

 <li class="<?php if(isset($active6)){echo $active6;}?>">
                    <a href="user/user.php"><i class="fa fa-user"></i> <span>Usuarios</span></a>
                </li>


    <li class="<?php if(isset($active4)){echo $active4;}?>">
                    <a href="tipocomida/tipocomida.php"><i class="fa fa-link"></i> <span>Tipo comida</span></a>
                </li>


                  <li class="<?php if(isset($active3)){echo $active3;}?>">
                    <a href="restaurante/restaurante.php"><i class="fa-link"></i> <span>Restaurantes</span></a>
                </li>

                <li class="<?php if(isset($active2)){echo $active2;}?>">
                    <a href="reserva/reserva.php"><i class="fa fa-link"></i> <span>Reservas</span></a>
                </li>

              

            </ul>
        </section><!-- /.sidebar -->
    </aside>
    
