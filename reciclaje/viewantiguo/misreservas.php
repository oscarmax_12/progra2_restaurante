
<?php 
	 require_once ("php/conexion.php");
     session_start();
     $tipo_usuario = 0;
 

     if($tipo_usuario==1){
		header("location: login_restaurante.php");
	}

	$idusuario = "";

	if (isset($_SESSION['id'])) {
        $idusuario = $_SESSION['id'];
		              
	}


     if(isset($_POST['p'])) $p=$_POST['p'];
		else $p=1; //pagina ver


		$crpp=1;

	$nombre = "";

	if (isset($_SESSION['nombre'])) {
        $nombre = $_SESSION['nombre'];             
	}

	$apellido = "";

	if (isset($_SESSION['apellido'])) {
        $apellido = $_SESSION['apellido'];            
	}


	$foto = "";
	if (isset($_SESSION['foto'])) {
        $foto = $_SESSION['foto'];              
	}

	$email = "";
	if (isset($_SESSION['email'])) {
        //asignar a variable
        $email = $_SESSION['email'];              
	}

	$sql = "SELECT count(DISTINCT r.id) as cantidad FROM reserva r inner join restaurante re on r.idrestaurante=re.id inner join usuarios u  on r.idusuario=u.id WHERE u.id='$idusuario'";

	$result = $cnx->query($sql) or die("error");
	$reg=$result->fetchObject();
	$cantreg=$reg->cantidad;
	$cantpag=ceil($cantreg/$crpp);
	$inicio=($p-1)*$crpp;



?>


<!DOCTYPE html>
<html>
<head>
<title>Food_Template Bootstrap Responsive Website Template | order page :: w3layouts</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Custom Theme files -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfont-->
<link href='//fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Lobster+Two:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<!--Animation-->
<script src="js/wow.min.js"></script>
<link href="css/animate.css" rel='stylesheet' type='text/css' />
<script>
	new WOW().init();
</script>
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
				});
			});
		</script>
<script src="js/jquery.carouFredSel-6.1.0-packed.js"></script>
<script src="js/tms-0.4.1.js"></script>
<script>
 $(window).load(function(){
      $('.slider')._TMS({
              show:0,
              pauseOnHover:false,
              prevBu:'.prev',
              nextBu:'.next',
              playBu:false,
              duration:800,
              preset:'fade', 
              pagination:true,//'.pagination',true,'<ul></ul>'
              pagNums:false,
              slideshow:8000,
              numStatus:false,
              banners:false,
          waitBannerAnimation:false,
        progressBar:false
      })  
      });
      
     $(window).load (
    function(){$('.carousel1').carouFredSel({auto: false,prev: '.prev',next: '.next', width: 960, items: {
      visible : {min: 1,
       max: 4
},
height: 'auto',
 width: 240,

    }, responsive: false, 
    
    scroll: 1, 
    
    mousewheel: false,
    
    swipe: {onMouse: false, onTouch: false}});
    
    
    });      

     </script>
<script src="js/jquery.easydropdown.js"></script>
<script src="js/simpleCart.min.js"> </script>
</head>
<body>
    <!-- header-section-starts -->
	<div class="header">
		<div class="container">
			<div class="top-header">
				<div class="logo">
					<a href="index.php"><img src="images/logo.png" class="img-responsive" alt="" /></a>
				</div>
				<div class="queries">
					<p>Questions? Call us Toll-free!<span>1800-0000-7777 </span><label>(11AM to 11PM)</label></p>
				</div>
				<div class="header-right">
						<div class="cart box_1">
							
							<?php 
								if (!isset($_SESSION['id'])){
										echo '';
									};
								?> 
									
								<?php 
									if (isset($_SESSION['id'])){
										echo '<h3> Bienvenido(a): '.$nombre.'
												<img src="'.$foto.'" style="width: 100px"></h3>
												<a href="checkout.html">
												</a>';
								};
							?> 

							<div class="clearfix"> </div>
						</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
			<div class="menu-bar">
			<div class="container">
				<div class="top-menu">
					<ul>
						<li class="active"><a href="index.php" class="scroll">Home</a></li>|
						<li><a href="restaurants.php">Restaurantes</a></li>|
						<li><a href="order.html">Reserva</a></li>|
						<li><a href="contact.html">contacto</a></li>
						<div class="clearfix"></div>
					</ul>
				</div>
				<div class="login-section">
					<ul>
						
						<li><a href="login_usuario.php">Login usuario</a>  </li> 
						<li><a href="login_restaurante.php">Login Restaurante</a>  </li> 

						<li><a href="cerrarsesion.php">Salir</a>  </li> 
						<div class="clearfix"></div>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		</div>
	<!-- header-section-ends -->
	<div class="order-section-page">
		<div class="ordering-form">
			<div class="container">
			<div class="order-form-head text-center wow bounceInLeft" data-wow-delay="0.4s">
						<h3>Mis Reservas</h3>
						
						<div class="container">
				<?php
				require_once ("php/conexion.php");
				$sql = "SELECT r.fecha,r.hora,r.cantidadpersonas, re.nombre restaurante FROM reserva r inner join restaurante re on r.idrestaurante=re.id inner join usuarios u  on r.idusuario=u.id WHERE u.id='$idusuario' LIMIT $inicio,$crpp";

				$result = $cnx->query($sql) or die("error");

				while($reg=$result->fetchObject()){
					echo "
					<div class='cart-header'>
				 <div class='close1'> </div>
				 <div class='cart-sec simpleCart_shelfItem'>
					   <div class='cart-item-info'>
						<h3><a href='#'>$reg->restaurante</a><span>Fecha: $reg->fecha</span></h3>
						<ul class='qty'>
							<li><p>Hora: $reg->hora</p></li>
							<li><p>Cant. personas: $reg->cantidadpersonas</p></li>
						</ul>
							 <div class='delivery'>
							 <p>Service Charges : $10.00</p>
							 <span>Delivered in 1-1:30 hours</span>
							 <div class='clearfix'></div>
				        </div>	
					   </div>
					   <div class='clearfix'></div>
											
				  </div>
			 </div>";
					}

				?>

			</div>
						

			</div>


			<div  class="container" id='divpaginador'>
			<ul class='pagination pagination-sm'>
				<li class='page-item disabled'>
					<a class='page-link' href='#' tabindex='-1'>Anterior</a>
			</li>
				<?php
				$cad="";
				for($i=1;$i<=$cantpag;$i++){
				$cad.= "<li onclick='verpag($i)' class='page-item'><a class='page-link'>$i</a></li>";
					}
				echo $cad;
			    ?>
			<li class='page-item'>
				<a class='page-link'>Siguiente</a>
			</li>
			</ul>
		</div>

		<div id="resultados">
			<div class="container">
				<div class="queries">
					<div class="Popular-Restaurants-content">
							<h4><?php echo "Mostrándo ".$crpp." de ".$cantreg?></h4>
					</div>
				</div>
			</div>
		</div>

			</div>
		</div>
<div class="special-offers-section">
			<div class="container">
				<div class="special-offers-section-head text-center dotted-line">
					<h4>Special Offers</h4>
				</div>
				<div class="special-offers-section-grids">
				 <div class="m_3"><span class="middle-dotted-line"> </span> </div>
				   <div class="container">
					  <ul id="flexiselDemo3">
						<li>
							<div class="offer">
								<div class="offer-image">	
									<img src="images/p1.jpg" class="img-responsive" alt=""/>
								</div>
								<div class="offer-text">
									<h4>Olister Combo pack lorem</h4>
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. </p>
									<input type="button" value="Grab It">
									<span></span>
								</div>
								<div class="clearfix"></div>
							</div>
						</li>
						<li>
							<div class="offer">
								<div class="offer-image">	
									<img src="images/p2.jpg" class="img-responsive" alt=""/>
								</div>
								<div class="offer-text">
									<h4>Chicken Jumbo pack lorem</h4>
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. </p>
									<input type="button" value="Grab It">
									<span></span>
								</div>
								<div class="clearfix"></div>
							</div>
						</li>
						<li>
							<div class="offer">
								<div class="offer-image">	
									<img src="images/p1.jpg" class="img-responsive" alt=""/>
								</div>
								<div class="offer-text">
									<h4>Crab Combo pack lorem</h4>
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. </p>
									<input type="button" value="Grab It">
									<span></span>
								</div>
								
								<div class="clearfix"></div>
								</div>
						</li>
						<li>
							<div class="offer">
								<div class="offer-image">	
									<img src="images/p2.jpg" class="img-responsive" alt=""/>
								</div>
								<div class="offer-text">
									<h4>Chicken Jumbo pack lorem</h4>
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. </p>
									<input type="button" value="Grab It">
									<span></span>
								</div>
								
								<div class="clearfix"></div>
								</div>
					    </li>
					 </ul>
				 <script type="text/javascript">
					$(window).load(function() {
						
						$("#flexiselDemo3").flexisel({
							visibleItems: 3,
							animationSpeed: 1000,
							autoPlay: true,
							autoPlaySpeed: 3000,    		
							pauseOnHover: true,
							enableResponsiveBreakpoints: true,
							responsiveBreakpoints: { 
								portrait: { 
									changePoint:480,
									visibleItems: 1
								}, 
								landscape: { 
									changePoint:640,
									visibleItems: 2
								},
								tablet: { 
									changePoint:768,
									visibleItems: 3
								}
							}
						});
						
					});
				    </script>
				    <script type="text/javascript" src="js/jquery.flexisel.js"></script>
				</div>
			</div>
		</div>
		</div>
	</div>
	<!-- footer-section-starts -->
	<div class="footer">
		<div class="container">
			<p class="wow fadeInLeft" data-wow-delay="0.4s">&copy; 2014  All rights  Reserved | Template by &nbsp;<a href="http://w3layouts.com" target="target_blank">W3Layouts</a></p>
		</div>
	</div>
	<!-- footer-section-ends -->
	  <script type="text/javascript">
						$(document).ready(function() {
							/*
							var defaults = {
					  			containerID: 'toTop', // fading element id
								containerHoverID: 'toTopHover', // fading element hover id
								scrollSpeed: 1200,
								easingType: 'linear' 
					 		};
							*/
							
							$().UItoTop({ easingType: 'easeOutQuart' });
							
						});
					</script>
				<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

	<script src="js/funciones.js"></script>

</body>
</html>