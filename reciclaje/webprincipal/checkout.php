<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->


<?php

require_once ("php/conexion.php");

session_start();

$tipo_usuario = 0;
	if (isset($_SESSION['tipo_usuario'])) {
        $tipo_usuario = $_SESSION['tipo_usuario'];            
	}
	if($tipo_usuario==1){
		header("location: login_restaurante.php");
	}

	$idusuario = "";

	if (isset($_SESSION['id'])) {
        $idSesion = $_SESSION['id'];          
	}


	$nombre = "";

	if (isset($_SESSION['nombre'])) {
        $nombre = $_SESSION['nombre'];             
	}

	$apellido = "";

	if (isset($_SESSION['apellido'])) {
        $apellido = $_SESSION['apellido'];            
	}


	$foto = "";
	if (isset($_SESSION['foto'])) {
        $foto = $_SESSION['foto'];              
	}

	$email = "";
	if (isset($_SESSION['email'])) {
        //asignar a variable
        $email = $_SESSION['email'];              
	}


if(isset($_GET['txttipocomida'])) $tipocomida=$_GET['txttipocomida'];
else $tipocomida = "";

if(isset($_POST['p'])) $p=$_POST['p'];
else $p=1; //pagina ver

$crpp=3;

$sql = "SELECT count(*) as cantidad,r.id, r.fecha, r.hora,r.cantidadpersonas, re.nombre restaurante, u.nombre usuarios,re.imagen 
FROM reserva r inner join restaurante re on r.idrestaurante=re.id inner join usuarios u on r.idusuario=u.id
";

$result = $cnx->query($sql) or die("error");
$reg=$result->fetchObject();
$cantreg=$reg->cantidad;
$cantpag=ceil($cantreg/$crpp);
$inicio=($p-1)*$crpp;
?>



<html>
<head>
<title>Food_Template Bootstrap Responsive Website Template | Checkout :: w3layouts</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- Custom Theme files -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfont-->
<link href='//fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Lobster+Two:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<!--Animation-->
<script src="js/wow.min.js"></script>
<link href="css/animate.css" rel='stylesheet' type='text/css' />
<script>
	new WOW().init();
</script>
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
				});
			});
		</script>		
<script src="js/simpleCart.min.js"> </script>	
</head>
<body>
    <!-- header-section-starts -->
	<div class="header">
		<div class="container">
			<div class="top-header">
				<div class="logo">
					<a href="index.html"><img src="images/logo.png" class="img-responsive" alt="" /></a>
				</div>
				<div class="queries">
					<p>Questions? Call us Toll-free!<span>1800-0000-7777 </span><label>(11AM to 11PM)</label></p>
				</div>
				<div class="header-right">
						<div class="cart box_1">



							<?php 
								if (!isset($_SESSION['id'])){
										echo '';
									};
								?> 
									
								<?php 
									if (isset($_SESSION['id'])){
										echo '<h3> Bienvenido(a): '.$nombre.'
												<img src="'.$foto.'" style="width: 100px"></h3>
												<a href="checkout.html">
												</a>';
								};
							?> 



							<a href="checkout.html">
								<h3> <span class="simpleCart_total"> $0.00 </span> (<span id="simpleCart_quantity" class="simpleCart_quantity"> 0 </span> items)<img src="images/bag.png" alt=""></h3>
							</a>	
							<p><a href="javascript:;" class="simpleCart_empty">Empty card</a></p>
							<div class="clearfix"> </div>
						</div>
					</div>
				<div class="clearfix"></div>
			</div>
		</div>
			<div class="menu-bar">
			<div class="container">
				<div class="top-menu">
					<ul>
						<li><a href="index.html">Home</a></li>|
						<li class="active"><a href="restaurants.html">Popular Restaurants</a></li>|
						<li><a href="order.html">Order</a></li>|
						<li><a href="contact.html">contact</a></li>
						<li><a href="misreservas.php">Mis reservas</a></li>
						<div class="clearfix"></div>
					</ul>
				</div>
				<div class="login-section">
					<ul>
						<li><a href="login.html">Login</a>  </li> |
						<li><a href="register.html">Register</a> </li> |
						<li><a href="#">Help</a></li>
						<div class="clearfix"></div>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>		
				</div>


	<!-- header-section-ends -->
	<!-- content-section-starts -->
	<!-- checkout -->
<div class="cart-items">
	<div class="container">
			 <h1>Mis reservas</h1>
				<!--<script>$(document).ready(function(c) {
					$('.close1').on('click', function(c){
						$('.cart-header').fadeOut('slow', function(c){
							$('.cart-header').remove();
						});
						});	  
					});
			   </script>-->
			 <div class="cart-header">
				 <div class="close1"> </div>
				 <div class="cart-sec simpleCart_shelfItem">
						
					   <div class="cart-item-info">
						<h3><a href="#"> Lorem Ipsum is not simply </a><span>Pickup time:</span></h3>
						<ul class="qty">
							<li><p>Min. order value:</p></li>
							<li><p>FREE delivery</p></li>
						</ul>
							 <div class="delivery">
							 <p>Service Charges : $10.00</p>
							 <span>Delivered in 1-1:30 hours</span>
							 <div class="clearfix"></div>
				        </div>	
					   </div>
					   <div class="clearfix"></div>
											
				  </div>
			 </div>
			  		
		 </div>
		 </div>
<!-- checkout -->
	<div class="contact-section" id="contact">
			<div class="container">
				<div class="contact-section-grids">
					<div class="col-md-3 contact-section-grid wow fadeInLeft" data-wow-delay="0.4s">

						<?php
				require_once ("php/conexion.php");
				$sql = "SELECT count(*) as cantidad,r.id, r.fecha, r.hora,r.cantidadpersonas, re.nombre restaurante, u.nombre usuarios,re.imagen 
FROM reserva r inner join restaurante re on r.idrestaurante=re.id inner join usuarios u on r.idusuario=u.id
LIMIT $inicio,$crpp";
				$result = $cnx->query($sql) or die("error");
				while($reg=$result->fetchObject()){
					echo "<form method='get' action='reservar.php'>
						<div class='Popular-Restaurants-grid wow fadeInRight' data-wow-delay='0.4s'>


					<div class='col-md-3 restaurent-logo'>
						<a href='./detail.php'>
						<img src='uploads/$reg->imagen' class='img-responsive' alt='' />
					</div>

					<div class='col-md-2 restaurent-title'>
						<div clas='logo-title'>
							<a name='txtidres' value='$reg->id'/>
						</div>
						<div class='logo-title'>
							<h4><a href='#''>$reg->nombre</a></h4>
						</div>
						<div class='rating'>
							<span>ratíngs</span>
							<a href='#''> <img src='images/star1.png' class='img-responsive' alt=''>(004)</a>
						</div>
					</div>
					<div class='col-md-7 buy'>
					
						<span>$45</span>
					</div>
					<div class='col-md-7 buy'>
						<a  class='morebtn hvr-rectangle-in' href='reservar.php?id=$reg->id'>Reservar</a>
						
					</div>

					<div class='clearfix'></div>
				</div>
				</form>";
					}

				?>
						<h4>Site Links</h4>
						<ul>
							<li><i class="point"></i></li>
							<li class="data"><a href="#">About Us</a></li>
						</ul>
						<ul>
							<li><i class="point"></i></li>
							<li class="data"><a href="#">Contact Us</a></li>
						</ul>
						<ul>
							<li><i class="point"></i></li>
							<li class="data"><a href="#">Privacy Policy</a></li>
						</ul>
						<ul>
							<li><i class="point"></i></li>
							<li class="data"><a href="#">Terms of Use</a></li>
						</ul>
					</div>
					<div class="col-md-3 contact-section-grid wow fadeInLeft" data-wow-delay="0.4s">
						<h4>Site Links</h4>
						<ul>
							<li><i class="point"></i></li>
							<li class="data"><a href="#">About Us</a></li>
						</ul>
						<ul>
							<li><i class="point"></i></li>
							<li class="data"><a href="#">Contact Us</a></li>
						</ul>
						<ul>
							<li><i class="point"></i></li>
							<li class="data"><a href="#">Privacy Policy</a></li>
						</ul>
						<ul>
							<li><i class="point"></i></li>
							<li class="data"><a href="#">Terms of Use</a></li>
						</ul>
					</div>
					<div class="col-md-3 contact-section-grid wow fadeInRight" data-wow-delay="0.4s">
						<h4>Follow Us On...</h4>
						<ul>
							<li><i class="fb"></i></li>
							<li class="data"> <a href="#">  Facebook</a></li>
						</ul>
						<ul>
							<li><i class="tw"></i></li>
							<li class="data"> <a href="#">Twitter</a></li>
						</ul>
						<ul>
							<li><i class="in"></i></li>
							<li class="data"><a href="#"> Linkedin</a></li>
						</ul>
						<ul>
							<li><i class="gp"></i></li>
							<li class="data"><a href="#">Google Plus</a></li>
						</ul>
					</div>
					<div class="col-md-3 contact-section-grid nth-grid wow fadeInRight" data-wow-delay="0.4s">
						<h4>Subscribe Newsletter</h4>
						<p>To get latest updates and food deals every week</p>
						<input type="text" class="text" value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = '';}">
						<input type="submit" value="submit">
						</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	<!-- content-section-ends -->
	<!-- footer-section-starts -->
	<div class="footer">
		<div class="container">
			<p class="wow fadeInLeft" data-wow-delay="0.4s">&copy; 2014  All rights  Reserved | Template by &nbsp;<a href="http://w3layouts.com" target="target_blank">W3Layouts</a></p>
		</div>
	</div>
	<!-- footer-section-ends -->
	  <script type="text/javascript">
						$(document).ready(function() {
							/*
							var defaults = {
					  			containerID: 'toTop', // fading element id
								containerHoverID: 'toTopHover', // fading element hover id
								scrollSpeed: 1200,
								easingType: 'linear' 
					 		};
							*/
							
							$().UItoTop({ easingType: 'easeOutQuart' });
							
						});
					</script>
				<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

</body>
</html>